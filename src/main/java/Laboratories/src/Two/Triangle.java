/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Two;

import java.util.Scanner;

/**
 * A Triangle class that determines the type of triangle inputted based on the value of each side.
 */
public class Triangle {

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Query the user to input the first side and then store the value
		System.out.print("Please enter the first side: ");
		int side1 = scanner.nextInt();
		// Query the user to input the second side and then store the value
		System.out.print("Please enter the second side: ");
		int side2 = scanner.nextInt();
		// Query the user to input the third side and then store the value
		System.out.print("Please enter the third side: ");
		int side3 = scanner.nextInt();
		// Check that no side of the triangle has a greater sum than the other sides
		if (side1 > side2 + side3 || side2 > side1 + side3 || side3 > side1 + side2) {
			// Throw an exception
			throw new IllegalArgumentException("One side of the triangle is larger than the sum of the other two.");
		} else {
			// Check if all the sides are equal
			if (side1 == side2 && side2 == side3) {
				// Output that it is an equilateral triangle
				System.out.print("Equilateral triangle");
			}
			// Check if two sides are equal
			else if (side1 == side2 || side1 == side3 || side2 == side3) {
				// Output that it is an isosceles triangle
				System.out.print("Isosceles triangle");
			}
			else {
				// Output that it is a scalene triangle
				System.out.print("Scalene triangle");
			}
		}
	}

}