/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Two;

import java.util.Arrays;
import java.util.Scanner;

/**
 * A simple class that determines what colour the user inputted.
 */
public class SwitchErrors {

	/**
	 * A Colour enumeration that defines the set of colours available.
	 */
	public enum Colour {
		BLUE,
		RED,
		GREEN;

		Colour() {}

		@Override
		public String toString() {
			String name = this.name();
			return name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
		}
	}

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Output the key heading
		System.out.println("Key:");
		// Iterate through every Colour enumeration and display their key value
		Arrays.asList(Colour.values())
			.stream()
			.forEach(c -> System.out.printf("\t%d. %s\n", c.ordinal() + 1, c.toString()));
		// Query the user to input the number and store the value
		System.out.print("Enter a number and I'll return the corresponding colour: ");
		int number = scanner.nextInt();
		// Attempt to get the correct colour
		try {
			// Display the colour using the index chosen by the user
			System.out.printf("You chose %s!\n", Colour.values()[number - 1].toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			// An error occurred and the colour is not available
			System.out.println("Colour not available!");
		}
	}

}