/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Two;

import java.util.Arrays;
import java.util.Scanner;

/**
 * A simple Salary class that asks the user for their normal and extra hours, factoring in a bonus, and then calculates
 * their total pay.
 */
public class Salary {

	/**
	 * Private member variables to hold the base and extra rate.
	 */
	private static final double NORMAL_RATE = 10;
	private static final double EXTRA_RATE = 15;

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Query the user to input their normal hours and then store the value
		System.out.print("Please enter number of normal hours: ");
		double normal = scanner.nextDouble();
		// Query the user to input their extra hours and then store the value
		System.out.print("Please enter number of extra hours: ");
		double extra = scanner.nextDouble();
		// Calculate the base total salary and create an array to mutate the total
		double base = normal * NORMAL_RATE + extra * EXTRA_RATE;
		final double[] total = {base};
		// Get the enumeration values and filter them based on the threshold values and then apply the bonus if it exists
		Arrays.asList(Laboratories.src.Salary.Information.values())
			.stream()
			.filter(s -> base <= s.getMax() && base >= s.getMin())
			.forEach(s -> total[0] *= s.getBonus());
		// Output the salary
		System.out.printf("Total salary is: $%.2f \n", total[0]);
	}

}
