/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Two;

import Laboratories.src.Salary;

import java.util.Arrays;
import java.util.Scanner;

/**
 * A simple Salary class that asks the user for their normal and extra hours, factoring in a bonus, and then calculates
 * their total pay on a weekly basis.
 */
public class SalaryWeek {

	/**
	 * Private member variables to hold the base and extra rate.
	 */
	private static final double NORMAL_RATE = 10;
	private static final double EXTRA_RATE = 15;

	public static void main (String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Store the normal and extra hours
		double  normal = 0,
				extra = 0;
		// Query the user to input how many weeks they are calculating their salary for and store the value
		System.out.print("How many weeks are you calculating your salary for? ");
		int weeks = scanner.nextInt();
		// Iterate through al the weeks
		for (int i = 1; i <= weeks; i++) {
			System.out.printf("Week %s\n", i);
			// Query the user to input their normal hours and then append the value
			System.out.print("Please enter number of normal hours: ");
			normal += scanner.nextDouble();
			// Query the user to input their extra hours and then append the value
			System.out.print("Please enter number of extra hours: ");
			extra += scanner.nextDouble();
		}
		// Calculate the base total salary and create an array to mutate the total
		double base = normal * NORMAL_RATE + extra * EXTRA_RATE;
		final double[] total = {base};
		// Get the enumeration values and filter them based on the threshold values and then apply the bonus if it exists
		Arrays.asList(Salary.Information.values())
			.stream()
			.filter(s -> base <= s.getMax() && base >= s.getMin())
			.forEach(s -> total[0] *= s.getBonus());
		// Output the salary
		System.out.printf("Total salary is: $%.2f \n", total[0]);
	}

}
