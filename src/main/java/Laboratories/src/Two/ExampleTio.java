/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Two;

import java.util.Scanner;

/**
 * A simple class that handles string manipulation of two first names and a last name.
 */
public class ExampleTio {

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Query the user to input the first first name and store the value
		System.out.print("Please enter the first first name: ");
		String first1 = scanner.next();
		// Query the user to input the second first name and store the value
		System.out.print("Please enter the second first name: ");
		String first2 = scanner.next();
		// Query the user to input the last name and store the value
		System.out.print("Please enter the last name: ");
		String last = scanner.next();
		// Manipulate the string and output it
		System.out.printf("%s %s and %s %s",
			first1.toUpperCase(),
			last.substring(0, 1).toUpperCase() + last.substring(1).toLowerCase(),
			first2.substring(0, 1).toUpperCase() + first2.substring(1).toLowerCase(),
			last.toUpperCase()
		);
	}

}

