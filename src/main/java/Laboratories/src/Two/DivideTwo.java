/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Two;

import java.util.Scanner;

/**
 * A simple Division class that asks the user for two values and then divides them. If the denominator is zero then the
 * program will provide an error message.
 */
public class DivideTwo {

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Output the program heading
		System.out.println("This program divides two numbers.");
		// Query the user to input the numerator and store the value
		System.out.print("Enter the numerator: ");
		double numerator = scanner.nextDouble();
		// Query the user to input the denominator and store the value
		System.out.print("Enter the denominator: ");
		double denominator = scanner.nextDouble();
		// Check if the denominator was zero
		if (denominator == 0) {
			System.out.println("Cannot divide by zero.");
		} else {
			// Output the division
			System.out.printf("%s / %s = %.2f", numerator, denominator, numerator / denominator);
		}
	}

}