/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.Agency;

/**
 * A Person class that defines the attributes and abilities of a person.
 */
public class Person implements Comparable<Person> {

	public enum Sex {
		MALE,
		FEMALE
	}

	/**
	 * Private member variables.
	 */
	private String name;	// The name of the person.
	private Sex sex;		// The sex of the person.
	private int age;		// The age of the person.

	/**
	 * @constructor
	 * The default constructor that sets the member variables to their default values.
	 */
	public Person() {
		this.name = "";
		this.sex = Sex.MALE;
		this.age = 0;
	}

	/**
	 * @constructor
	 * A constructor that sets the member variables of the person.
	 *
	 * @param name The name of the person.
	 * @param sex The sex of the person.
	 * @param age The age of a person.
	 */
	public Person(String name, Sex sex, int age) {
		this();
		this.name = name;
		this.sex = sex;
		this.age = age;
	}

	/**
	 * A mutator method that sets the name of the person.
	 *
	 * @param name The name of the person.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the person.
	 *
	 * @return The name of the person.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * A mutator method that sets the sex of the person.
	 *
	 * @param sex The sex of the person.
	 */
	public void setSex(Sex sex) {
		this.sex = sex;
	}

	/**
	 * An accessor method that returns the sex of the person.
	 *
	 * @return The sex of the person.
	 */
	public Sex getSex() {
		return this.sex;
	}

	/**
	 * A mutator method that sets the age of the person.
	 *
	 * @param age The age of the person.
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * An accessor method that return the age of the person.
	 *
	 * @return The age of the person.
	 */
	public int getAge() {
		return this.age;
	}

	@Override
	public String toString() {
		return String.format("%s:\n\tName: %s\n\tAge: %d\n",
				this.getSex() == Sex.MALE ? "Him" : "Her",
				this.getName(),
				this.getAge()
		);
	}

	/**
	 * Determines and returns if the people are the same person
	 *
	 * @param person the person being compared
	 * @return whether the people being compared are the same person
	 */
	@Override
	public int compareTo(Person person) {
		int names = this.getName().compareTo(person.getName());
		int ages = Integer.compare(this.getAge(), person.getAge());
		return names == 0 && ages == 0 ? 0 : 1;
	}

}