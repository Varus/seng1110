/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.Agency;

/**
 * A class that defines a heterosexual Couple as two Persons.
 */
public class Couple {

	/**
	 * Member variables
	 */
	private Person she;
	private Person he;

	/**
	 * @constructor
	 * The default constructor that instantiates two Persons.
	 */
	public Couple() {
		this.she = new Person();
		this.he = new Person();
	}

	/**
	 * @constructor
	 * A constructor that sets the member variables.
	 *
	 * @param she The female within the couple.
	 * @param he The male within the couple.
	 */
	public Couple(Person she, Person he) {
		this.she = she;
		this.he  = he;
	}

	/**
	 * An accessor method that returns the female within the couple.
	 *
	 * @return The female within the couple.
	 */
	public Person getShe() {
		return this.she;
	}

	/**
	 * An accessor method that returns the male within the couple.
	 *
	 * @return The male within the couple.
	 */
	public Person getHe() {
		return this.he;
	}

	/**
	 * Tests whether the female is younger than the male within the couple.
	 *
	 * @return Whether the female is younger than the male or not.
	 */
	public boolean test() {
		return she.getAge() < he.getAge();
	}

	@Override
	public String toString() {
		return String.format("Her:\n\tName: %s\n\tAge: %s\nHim:\n\tName: %s\n\tAge: %s\n",
			she.getName(),
			she.getAge(),
			he.getName(),
			he.getAge()
		);
	}

}