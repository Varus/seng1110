/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.Agency;

import java.util.Scanner;
import Laboratories.src.Five.Agency.Person.Sex;

/**
 * A class that creates heterosexual Couples, compares their ages and checks whether the same member exists in other
 * couples.
 */
public class AgencyInterface {

	/**
	 * Private member variables.
	 */
	private Scanner scanner = new Scanner(System.in);

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		AgencyInterface agencyInterface = new AgencyInterface();
		agencyInterface.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	private void start(String[] args) {
		// Create two couples
		Couple couple1 = this.createCouple();
		Couple couple2 = this.createCouple();
		// Test both couples
		System.out.printf("GOOD FOR %s!\n", couple1.test() ? couple1.getHe().getName() : couple1.getShe().getName());
		System.out.printf("GOOD FOR %s!\n", couple2.test() ? couple2.getHe().getName() : couple2.getShe().getName());
		// Check if the same female appears in both couples
		if (couple1.getShe().equals(couple2.getShe())) {
			System.out.println("Same female in different couples!");
		}
		// Checks if the same male appears in both couples
		if (couple1.getHe().equals(couple2.getHe())) {
			System.out.println("Same male in different couples!");
		}
	}

	/**
	 * Creates a new couple and returns it.
	 *
	 * @return The newly added couple.
	 */
	private Couple createCouple() {
		// Create a female and male person
		Person female = this.createPerson(Sex.FEMALE);
		Person male = this.createPerson(Sex.MALE);
		// Create the couple and return it
		return new Couple(female, male);
	}

	/**
	 * Creates and returns a person of a particular sex.
	 *
	 * @param sex The gender of the person being created.
	 * @return The person object.
	 */
	private Person createPerson(Sex sex) {
		// Query the user for the name of the person and store the value
		System.out.printf("%s name: ", sex == Sex.MALE ? "His" : "Her");
		String name = scanner.next();
		// Query the user for the age of the person and store the value
		System.out.print("Age: ");
		int age = scanner.nextInt();
		// Create and return the person object
		return new Person(name, sex, age);
	}

}