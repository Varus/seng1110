/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.Subjects;

import java.util.Scanner;

/**
 * An interface to create a student, their subjects and respective assignment and exam marks.
 */
public class StudentInterface {

	/**
	 * Private member variables.
	 */
	private Scanner scanner = new Scanner(System.in);

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		StudentInterface studentInterface = new StudentInterface();
		studentInterface.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start(String[] args) {
		// Query the user for the name of the student and create the object
		System.out.print("What is the name of the student: ");
		Student student = new Student(scanner.nextLine());
		// Query the user for how many subjects they are undertaking and store the value
		System.out.print("How many subjects are you undertaking? ");
		int subjects = scanner.nextInt();
		// Iterate through every subject
		for (int i = 0; i < subjects; i++) {
			// Add the subject to the student
			student.addSubject(this.createSubject());
		}
		System.out.printf("The average of all marks is %s.\n", student.getAverage());
	}

	/**
	 * Creates and returns a subject.
	 *
	 * @return The subject that the student is undertaking.
	 */
	public Subject createSubject() {
		scanner.nextLine();
		// Query the user for the name of the subject and create the object
		System.out.print("What is the name of the subject? ");
		String name = scanner.nextLine();
		Subject subject = new Subject(name);
		// Query the user for how many assignments they are undertaking in the subject and store the value
		System.out.printf("How many assignments have you taken in %s? ", name);
		int assignments = scanner.nextInt();
		// Iterate through every assignment
		for (int i = 1; i <= assignments; i++) {
			// Query the user for their mark and store it in the subject
			System.out.printf("\n\t%d. What is your assignment mark? ", i);
			subject.addAssignment(scanner.nextDouble());
		}
		// Query the user for how many exams they are undertaking in the subject and store the value
		System.out.printf("\nHow many exams have you taken in %s? ", name);
		int exams = scanner.nextInt();
		// Iterate through every exam
		for (int i = 1; i <= exams; i++) {
			// Query the user for their mark and store it in the subject
			System.out.printf("\n\t%d.What is your exam mark? ", i);
			subject.addExam(scanner.nextDouble());
		}
		System.out.println();
		return subject;
	}
}
