/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.Subjects;

import java.util.ArrayList;
import java.util.List;

/**
 * A Student class that stores their name and list of subjects that they are undertaking. This class is capable of
 * computing the total average of the student's subjects.
 */
public class Student {

	/**
	 * Private Member variables.
	 */
	private String name;			// The name of the student.
	private List<Subject> subjects;	// The list of subjects the student is taking.

	/**
	 * @constructor
	 * The default constructor that instantiates the default values for the private member variables.
	 */
	public Student() {
		this.name = "";
		this.subjects = new ArrayList<>();
	}

	/**
	 * @constructor
	 * A constructor that sets the name of the student.
	 *
	 * @param name The name of the student.
	 */
	public Student(String name) {
		this();
		this.name = name;
	}

	/**
	 * Adds a subject to the student's list of subjects that they are undertaking.
	 *
	 * @param subject The subject being undertaken by the student.
	 */
	public void addSubject(Subject subject) {
		this.subjects.add(subject);
	}

	/**
	 * Calculates and returns the average of all subjects.
	 *
	 * @return The average of all subjects.
	 */
	public double getAverage() {
		return subjects.stream().mapToDouble(Subject::getAverage).average().getAsDouble();
	}

}