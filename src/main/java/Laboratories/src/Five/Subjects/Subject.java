/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.Subjects;

import java.util.ArrayList;
import java.util.List;

/**
 * A Subject class that is used for a Student to keep track of their assignment and exam marks.
 */
public class Subject {

	/**
	 * Private member variables.
	 */
	private String name;
	private List<Double> assignments;
	private List<Double> exams;

	private static final double ASSIGNMENT_WEIGHT = 1;//0.1;
	private static final double EXAM_WEIGHT = 1;//0.35;

	/**
	 * @constructor
	 * The default constructor that initialises the default values of the private member variables.
	 */
	public Subject() {
		this.name = "";
		this.assignments = new ArrayList<>();
		this.exams = new ArrayList<>();
	}

	/**
	 * @constructor
	 * A constructor that sets the name of the subject.
	 *
	 * @param name The name of the subject.
	 */
	public Subject(String name) {
		this();
		this.name = name;
	}

	/**
	 * A mutator method that sets the name of the subject.
	 *
	 * @param name The name of the subject.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the subject.
	 *
	 * @return The name of the subject.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Adds an assignment mark to the subject.
	 *
	 * @param assignment The assignment mark.
	 */
	public void addAssignment(double assignment) {
		this.assignments.add(assignment);
	}

	/**
	 * Adds an exam mark to the subject.
	 *
	 * @param exam The exam mark.
	 */
	public void addExam(double exam) {
		this.exams.add(exam);
	}

	/**
	 * Calculates and returns the average of all assignment and exam marks.
	 *
	 * @return The average of all marks.
	 */
	public double getAverage() {
		double assignmentTotal = this.assignments.stream().mapToDouble(a -> a * ASSIGNMENT_WEIGHT).sum();
		double examTotal = this.exams.stream().mapToDouble(e -> e * EXAM_WEIGHT).sum();
		return (assignmentTotal + examTotal) / (this.assignments.size() + this.exams.size());
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("Subject: %s\n\n", this.getName()));
		this.assignments.forEach(a -> stringBuilder.append(String.format("Assignment %d: %.2f", this.assignments.indexOf(a) + 1, a)));
		this.exams.forEach(e -> stringBuilder.append(String.format("Exam %d: %.2f", this.exams.indexOf(e) + 1, e)));
		return stringBuilder.toString();
	}

}