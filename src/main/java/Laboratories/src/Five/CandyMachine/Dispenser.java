/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.CandyMachine;

/**
 * A Dispenser class that keeps track of its item count and cost for purchase.
 */
public class Dispenser {

	/**
	 * Private member variables.
	 */
	private String name;			// The name of the items that the dispenser carries.
	private int items; 	  			// The number of items in the dispenser.
	private int cost;    	   	  	// The cost of the item.
	private static int total = 0; 	// The total amount in the dispenser.

	private final static int DEFAULT_ITEMS = 50;
	private final static int DEFAULT_COST = 50;

	/**
	 * @constructor
	 * The default constructor that sets the default member variable values.
	 */
	public Dispenser() {
		this.name = "";
		this.items = DEFAULT_ITEMS;
		this.cost = DEFAULT_COST;
		total += 50;
	}

	/**
	 * @constructor
	 * A constructor that sets its name, number of items and cost of the item.
	 *
	 * @param name The name of items being dispensed.
	 * @param items The number of items in the dispenser.
	 * @param cost The cost to obtain the item within the dispenser.
	 */
	public Dispenser(String name, int items, int cost) {
		this.name = name;
		this.items = items >= 0 ? items : DEFAULT_ITEMS;
		this.cost = cost >= 0 ? cost : DEFAULT_COST;
		total += this.items;
	}

	/**
	 * An accessor method that returns the name of the items being dispensed.
	 *
	 * @return The name of the items being dispensed.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * An accessor method that returns the number of items in the dispenser.
	 *
	 * @return The number of items in the dispenser.
	 */
	public int getCount() {
		return this.items;
	}

	/**
	 * An accessor method that returns the product cost.
	 *
	 * @return The product cost.
	 */
	public int getCost() {
		return this.cost;
	}

	/**
	 * A method that reduces the number of items by making a sale.
	 */
	public void makeSale() {
		this.items--;
	}

	@Override
	public String toString() {
		return this.getName();
	}

}
