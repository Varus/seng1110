/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.CandyMachine;

/**
 * A Cash Register class that is able to handle deposits.
 */
public class CashRegister {

	/**
	 * Private member variables.
	 */
	private int balance; 	// The amount of money in the cash register.

	private static final int DEFAULT_BALANCE= 500;

	/**
	 * @constructor
	 * The default constructor that sets the default values of the private member variables.
	 */
	public CashRegister() {
		this.balance = DEFAULT_BALANCE;
	}

	/**
	 * @constructor
	 * A constructor that sets the amount of money within the cash register.
	 *
	 * @param balance The amount of money within the cash register.
	 */
	public CashRegister(int balance) {
		this.balance = balance >= 0 ? balance: DEFAULT_BALANCE;
	}

	/**
	 * An accessor method that returns the current balance in the register.
	 *
	 * @return The current amount of cash in the register.
	 */
	public int getBalance() {
		return this.balance;
	}

	/**
	 * Deposits money into the cash register.
	 *
	 * @param amount The amount to deposit into the cash register.
	 */
	public void deposit(int amount) {
		this.balance += amount;
	}

}

