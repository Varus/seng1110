/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Five.CandyMachine;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * A Candy Machine class that enables users to dispense various candies and deposit the money into the cash register.
 */
public class CandyMachine {

	/**
	 * Private member variables.
	 */
	private Scanner scanner = new Scanner(System.in);
	private List<Dispenser> dispensers = new ArrayList<>();

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		CandyMachine candyMachine = new CandyMachine();
		candyMachine.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start(String[] args) {
		// Instantiate the cash register
		CashRegister cashRegister = new CashRegister();
		// Add the dispensers to the list
		this.dispensers.add(new Dispenser("Candy", 100, 50));
		this.dispensers.add(new Dispenser("Chips", 100, 65));
		this.dispensers.add(new Dispenser("Gum", 75, 45));
		this.dispensers.add(new Dispenser("Cookies", 100, 85));
		// Define the lower and upper bound of the menu options
		int lowerBound = 1;
		int upperBound = this.dispensers.size() + 1;
		// Display the program heading
		System.out.printf("Welcome to One Stop Candy Shop!\n\n");
		// Loop until the user exits the program
		while (true) {
			// Display the menu and ask the user to select an option
			this.displayMenu();
			int option = this.getMenuOption(lowerBound, upperBound);
			// Check if the user chose to exit
			if (option == upperBound) {
				// Escape the loop
				break;
			}
			// Sell the relevant product
			this.sellProduct(this.dispensers.get(option - 1), cashRegister);
		}
	}

	/**
	 * Displays the menu options.
	 */
	public void displayMenu() {
		// Define the menu string format
		String format = "\t%d. %s\n";
		// Output the dispensers menu
		this.dispensers.forEach(d -> System.out.printf(format, this.dispensers.indexOf(d) + 1, d.toString()));
		// Display the exit option
		System.out.printf(format, this.dispensers.size() + 1, "Exit");
		// Display the choose an option clause
		System.out.print("\nPlease choose an option: ");
	}

	/**
	 * This method retrieves a valid menu option from the user
	 *
	 * @param lowerBound the minimum value for the menu option
	 * @param upperBound the maximum value for the menu option
	 * @return the menu option chosen by the user
	 */
	private int getMenuOption(int lowerBound, int upperBound) {
		if (upperBound <= lowerBound) {
			throw new RuntimeException("Bad function call: getMenuOption(). Upper bound must be greater than lower bound.");
		}
		while (true) {
			try {
				int i = scanner.nextInt();
				if (i < lowerBound || i > upperBound) {
					throw new InputMismatchException(String.format("The input you have entered is not between %d and %d.", lowerBound, upperBound));
				}
				return i;
			} catch(InputMismatchException e) {
				String message = e.getMessage();
				System.out.println(message == null ? "Please enter an integer value." : message);
				scanner.nextLine();
			}
		}
	}

	/**
	 * Asks the user to deposit money into the machine to sell and receive the product
	 *
	 * @param product the product being dispensed by the machine
	 * @param cashRegister the cash register within the machine
	 */
	public void sellProduct(Dispenser product, CashRegister cashRegister) {
		// Check if the product is available
		if (product.getCount() > 0) {
			// Gets the product cost, required and inserted coins
			int price = product.getCost();
			int coinsRequired = price;
			int coinsInserted = 0;
			// Make the user insert coins until the item has been paid
			while (coinsRequired > 0) {
				System.out.printf("Please deposit %s cents: ", coinsRequired);
				coinsInserted += scanner.nextInt();
				coinsRequired = price - coinsInserted;
			}
			// Deposit the total amount of coins into the cash register
			cashRegister.deposit(coinsInserted);
			// Make a sale on the product
			product.makeSale();
			// Output a message that the item is available
			System.out.println("Collect your item at the bottom and enjoy.\n");
		} else {
			System.out.println("Sorry this item is sold out.\n");
		}
	}

}
