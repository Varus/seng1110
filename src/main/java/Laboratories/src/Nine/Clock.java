/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Nine;

/**
 * A baseclass that defines the properties of a clock.
 */
public class Clock {

	/**
	 * Private member variables.
	 */
	private String name;	// The name of the clock.
	private int hour;		// The hour for the clock.
	private int minute;		// The minute for the clock.
	private int second;		// The second for the clock.

	/**
	 * @constructor
	 * The default constructor that initializes the default values of the member variables.
	 */
	public Clock() {
		this.name = "Clock";
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
	}

	/**
	 * @constructor
	 * A constructor that sets the hour, minute and second on the clock.
	 *
	 * @param hour The hour on the clock.
	 * @param minute The minute on the clock.
	 * @param second The second on the clock.
	 */
	public Clock(int hour, int minute, int second) {
		this();
		this.setHour(hour);
		this.setMinute(minute);
		this.setSecond(second);
	}

	/**
	 * @constructor
	 * A constructor that sets the name, hour, minute and second on the clock.
	 *
	 * @param hour The hour on the clock.
	 * @param minute The minute on the clock.
	 * @param second The second on the clock.
	 */
	public Clock(String name, int hour, int minute, int second) {
		this(hour, minute, second);
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the clock.
	 *
	 * @return The name of the clock.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * A mutator method that sets the hour of the clock if it is valid.
	 *
	 * @param hour The hour on the clock.
	 */
	public void setHour(int hour) {
		if (hour >= 0 && hour < 24) {
			this.hour = hour;
		}
	}

	/**
	 * An accessor method that returns the hour set on the clock.
	 *
	 * @return The hour on the clock.
	 */
	public int getHour() {
		return this.hour;
	}

	/**
	 * A mutator method that sets the minute if it is valid.
	 *
	 * @param minute The minute on the clock.
	 */
	public void setMinute(int minute) {
		if (minute >= 0 && minute < 60) {
			this.minute = minute;
		}
	}

	/**
	 * An accessor method that returns the minute on the clock.
	 *
	 * @return The minute on the clock.
	 */
	public int getMinute() {
		return this.minute;
	}

	/**
	 * A mutator method that sets the second if it is valid.
	 *
	 * @param second The second on the clock.
	 */
	public void setSecond(int second) {
		if (second >= 0 && second < 60) {
			this.second = second;
		}
	}

	/**
	 * An accessor method that returns the second on the clock.
	 *
	 * @return The second on the clock.
	 */
	public int getSecond() {
		return this.second;
	}

	/**
	 * An accessor method that returns the current time.
	 *
	 * @return The current time.
	 */
	public String getCurrentTime() {
		return this.toString();
	}

	@Override
	public String toString() {
		return String.format("%s: The current time is: %s:%s:%s",
				this.getName(),
				this.getHour(),
				this.getMinute(),
				this.getSecond()
		);
	}

	/**
	 * Determines if two clocks are the same
	 *
	 * @param o the clock being compared
	 * @return whether the two clocks are the same
	 */
	public boolean equals(Object o){
		if (o == null) {
			return false;
		} else {
			Clock clock = (Clock)o;
			return hour == clock.hour && minute == clock.minute && second == clock.second;
		}
	}

}