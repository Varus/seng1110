/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Nine;

/**
 * An Alarm Clock class that inherits a Clock.
 */
public class AlarmClock extends Clock {

	/**
	 * Private member variables.
	 */
	private int alarmHour;		// The hour for the alarm.
	private int alarmMinute;	// The minute for the alarm.
	private int alarmSecond;	// The second for the alarm.

	/**
	 * @constructor
	 * The default constructor that calls base class constructor and initializes the default values of the member
	 * variables.
	 */
	public AlarmClock() {
		super(0, 0, 0);
		this.alarmHour = 0;
		this.alarmMinute = 0;
		this.alarmSecond = 0;
	}

	/**
	 * @constructor
	 * A constructor that sets the time on the clock.
	 *
	 * @param hour The hour on the clock.
	 * @param minute The minute on the clock.
	 * @param second The second on the clock.
	 */
	public AlarmClock(int hour, int minute, int second) {
		super(hour, minute, second);
		this.alarmHour = 0;
		this.alarmMinute = 0;
		this.alarmSecond = 0;
	}

	/**
	 * @constructor
	 * A constructor that sets the time on the clock and its alarm.
	 *
	 * @param hour The hour on the clock.
	 * @param minute The minute on the clock.
	 * @param second The second on the clock.
	 * @param alarmHour The hour for the alarm.
	 * @param alarmMinute The minute for the alarm.
	 * @param alarmSecond The second for the alarm.
	 */
	public AlarmClock(int hour, int minute, int second, int alarmHour, int alarmMinute, int alarmSecond) {
		super(hour, minute, second);
		this.alarmHour = alarmHour;
		this.alarmMinute = alarmMinute;
		this.alarmSecond = alarmSecond;
	}

	/**
	 * @constructor
	 * A constructor that sets its name, the time and its alarm.
	 *
	 * @param name The name of the clock.
	 * @param hour The hour on the clock.
	 * @param minute The minute on the clock.
	 * @param second The second on the clock.
	 * @param alarmHour The hour for the alarm.
	 * @param alarmMinute The minute for the alarm.
	 * @param alarmSecond The second for the alarm.
	 */
	public AlarmClock(String name, int hour, int minute, int second, int alarmHour, int alarmMinute, int alarmSecond) {
		super(name, hour, minute, second);
		this.alarmHour = alarmHour;
		this.alarmMinute = alarmMinute;
		this.alarmSecond = alarmSecond;
	}

	/**
	 * A mutator method that sets the hour for the alarm.
	 *
	 * @param alarmHour The hour for the alarm.
	 */
	public void setAlarmHour(int alarmHour) {
		this.alarmHour = alarmHour;
	}

	/**
	 * An accessor method that returns the hour for the alarm.
	 *
	 * @return The hour for the alarm.
	 */
	public int getAlarmHour() {
		return this.alarmHour;
	}

	/**
	 * A mutator method that sets the minute for the alarm.
	 *
	 * @param alarmMinute The minute for the alarm.
	 */
	public void setAlarmMinute(int alarmMinute) {
		this.alarmMinute = alarmMinute;
	}

	/**
	 * An accessor method that returns the minute for the alarm.
	 *
	 * @return The minute for the alarm.
	 */
	public int getAlarmMinute() {
		return this.alarmMinute;
	}

	/**
	 * A mutator method that sets the second for the alarm.
	 *
	 * @param alarmSecond The second for the alarm.
	 */
	public void setAlarmSecond(int alarmSecond) {
		this.alarmSecond = alarmSecond;
	}

	/**
	 * An accessor method that returns the second for the alarm.
	 *
	 * @return The second for the alarm.
	 */
	public int getAlarmSecond() {
		return this.alarmSecond;
	}

	@Override
	public String toString() {
		return String.format("%s: The current time is: %s:%s:%s and the alarm is set at: %s:%s:%s",
			this.getName(),
			this.getHour(),
			this.getMinute(),
			this.getSecond(),
			this.getAlarmHour(),
			this.getAlarmMinute(),
			this.getAlarmSecond()
		);
	}

	/**
	 * Determines if two alarm clocks are the same.
	 *
	 * @param o the clock being compared.
	 * @return Whether the two alarm clocks are the same.
	 */
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (!(o instanceof AlarmClock)) {
			return false;
		} else {
			AlarmClock clock = (AlarmClock)o;
			return super.equals(clock) && alarmHour == clock.alarmHour &&
				alarmMinute == clock.alarmMinute && alarmSecond == clock.alarmSecond;
		}
	}

}
