/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Nine;

/**
 * A class that tests whether an alarm clock and clock are equivalent.
 */
public class ClockTest {

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		ClockTest clockTest = new ClockTest();
		clockTest.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	private void start(String[] args) {
		// Instantiate a clock with the time 12:30:00 and output it
		Clock clock = new Clock("Clock", 12, 30, 0);
		System.out.println(clock.toString());
		// Change the hour on the clock and output it
		clock.setHour(7);
		System.out.println(clock.toString());
		// Create another clock and output it
		Clock mySecondClock = new Clock("Second Clock", 7, 30, 0);
		System.out.println(mySecondClock.toString());
		// Compare the clock with the second clock
		this.compareClocks(clock, mySecondClock);
		// Create two alarm clocks and output them
		Clock alarmClock = new AlarmClock("Alarm Clock 1", 1, 45, 0, 6, 57, 0);
		Clock alarmClock2 = new AlarmClock("Alarm Clock 2", 1, 45, 0, 6, 57, 0);
		System.out.println(alarmClock.toString());
		System.out.println(alarmClock2.toString());
		// Compare the alarm clock with the clock
		this.compareClocks(alarmClock, clock);
		// Compare the alarm clock with the second alarm clock
		this.compareClocks(alarmClock, alarmClock2);
		// Set the clock's hour and minute
		clock.setHour(1);
		clock.setMinute(45);
		// Compare the clock with the alarm clock
		this.compareClocks(clock, alarmClock);
	}

	/**
	 * Compares two clock objects and outputs the result.
	 *
	 * @param clock1 The first clock.
	 * @param clock2 The second clock.
	 */
	private void compareClocks(Clock clock1, Clock clock2) {
		System.out.printf("The clocks are %sequal!\n", clock1.equals(clock2) ? "" : "not ");
	}

}