package Laboratories.src;

public class Salary {

    /**
     * An enumeration containing the Salary information which includes the bonus rate and the threshold (min and max)
     * needed to obtain it.
     */
    public static enum Information {

        ONE(0.1, 0, 500),
        TWO(0.05, 501, 1000);

        private final double bonus;
        private final double min;
        private final double max;

        Information(double bonus, double min, double max) {
            this.bonus = bonus;
            this.min = min;
            this.max = max;
        }

        public double getMin() {
            return this.min;
        }

        public double getMax() {
            return this.max;
        }

        public double getBonus() {
            return this.bonus + 1;
        }

    }

}
