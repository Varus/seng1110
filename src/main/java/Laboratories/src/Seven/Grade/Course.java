package Laboratories.src.Seven.Grade;

public class Course {

	/**
	 * Member variables
	 */
	private String name;
	private String id;
	private char grade;
	private int credits;

	/**
	 * Default constructor
	 */
	public Course() {
		name = "";
		id = "";
		grade = '*';
		credits = 0;
	}

	/**
	 * A constructor that sets the member variables
	 *
	 * @param name the name of the course
	 * @param id the id of the course
	 * @param grade the grade of the course
	 * @param credits the credits available for the course
	 */
	public Course(String name, String id, char grade, int credits) {
		this.name = name;
		this.id = id;
		this.grade = grade;
		this.credits = credits;
	}

	/**
	 * A mutator method that sets the name of the course
	 *
	 * @param name the name of the course
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the course
	 *
	 * @return the name of the course
	 */
	public String getName() {
		return name;
	}

	/**
	 * A mutator method that sets the id of the course
	 *
	 * @param id the id of the course
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * An accessor method that returns the id of the course
	 *
	 * @return the id of the course
	 */
	public String getId() {
		return id;
	}

	/**
	 * A mutator method that sets the grade of the course
	 *
	 * @param grade the grade of the course
	 */
	public void setGrade(char grade) {
		this.grade = grade;
	}

	/**
	 * An accessor method that returns the grade of the course
	 *
	 * @return the grade of the course
	 */
	public char getGrade() {
		return grade;
	}

	/**
	 * A mutator method that sets the credits for the course
	 *
	 * @param credits the credits available for the course
	 */
	public void setCredits(int credits) {
		this.credits = credits;
	}

	/**
	 * An accessor method to return the credits available for the course
	 *
	 * @return the credits available for the course
	 */
	public int getCredits() {
		return credits;
	}

	/**
	 * Returns the class string
	 *
	 * @param isPaid whether or not the tuition has been paid for or not
	 * @return the Course string
	 */
	public String toString(boolean isPaid) {
		return String.format("Course:\n\tName: %s\n\tId: %s\n\tGrade: %s\n\t%s", name, id, grade, isPaid ? credits : "");
	}

}
