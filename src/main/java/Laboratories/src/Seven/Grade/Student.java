package Laboratories.src.Seven.Grade;

public class Student {

	/**
	 * Member variables
	 */
	private final int MAX_COURSES = 6;
	private String firstName;
	private String lastName;
	private int id;
	private int numberOfCourses;
	private boolean isTuitionPaid;
	private Course [] coursesEnrolled;

	/**
	 * Default constructor
	 */
	public Student() {
		firstName = "";
		lastName = "";
		numberOfCourses = 0;
		id = 0;
		isTuitionPaid = false;
		coursesEnrolled = new Course[MAX_COURSES];
		for(int i = 0; i < coursesEnrolled.length; i++) {
			coursesEnrolled[i] = new Course();
		}
	}

	/**
	 * A constructor that sets the member variables
	 *
	 * @param firstName the first name of the student
	 * @param lastName the last name of the student
	 * @param id the student id
	 * @param numberOfCourses the number of courses the student has
	 * @param isTuitionPaid whether the tuition has been paid for
	 * @param coursesEnrolled the courses the student is enrolled in
	 */
	public Student(String firstName, String lastName, int id, int numberOfCourses, boolean isTuitionPaid, Course[] coursesEnrolled) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
		this.numberOfCourses = numberOfCourses;
		this.isTuitionPaid = isTuitionPaid;
		this.coursesEnrolled = coursesEnrolled;
	}

	/**
	 * A mutator method that sets the first and last name of a student
	 *
	 * @param firstName the first name of the student
	 * @param lastName the last name of the student
	 */
	public void setName(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * An accessor method that returns the first name of a student
	 *
	 * @return the first name of a student
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * An accessor method that returns the last name of a student
	 *
	 * @return the last name of a student
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * A mutator method that sets the id of a student
	 *
	 * @param id the id of a student
	 */
	public void setStudentId(int id) {
		this.id = id;
	}

	/**
	 * An accessor method that returns the id of a student
	 *
	 * @return the id of a student
	 */
	public int getId() {
		return id;
	}

	/**
	 * A mutator method that sets whether the tuition has been paid for or not
	 *
	 * @param isTuitionPaid determines if the tuition has been paid for
	 */
	public void setIsTuitionPaid(boolean isTuitionPaid) {
		this.isTuitionPaid = isTuitionPaid;
	}

	/**
	 * An accessor method that returns whether the tuition has been paid for or not
	 *
	 * @return whether the tuition has been paid for not
	 */
	public boolean isTuitionPaid() {
		return isTuitionPaid;
	}

	/**
	 * A mutator method that sets the number of courses being taken by the student
	 *
	 * @param numberOfCourses the number of courses the student has
	 */
	public void setNumberOfCourses(int numberOfCourses) {
		this.numberOfCourses = numberOfCourses ;
	}

	/**
	 * An accessor method that returns the number of courses the student has
	 *
	 * @return the number of courses the student has
	 */
	public int getNumberOfCourses() {
		return numberOfCourses;
	}

	/**
	 * An accessor method to return a copy of the selected course
	 *
	 * @param i the current course
	 * @return a copy of the course
	 */
	public Course getCourse(int i) {
		String name = coursesEnrolled[i].getName();
		String id = coursesEnrolled[i].getId();
		char grade = coursesEnrolled[i].getGrade();
		int credits = coursesEnrolled[i].getCredits();
		return new Course(name, id, grade, credits);
	}

	/**
	 * Calculates and returns the total amount of credits
	 *
	 * @return the total credits for all courses
	 */
	public int getHoursEnrolled() {
		int totalCredits = 0;

		for (int i = 0; i < numberOfCourses; i++) {
			totalCredits += coursesEnrolled[i].getCredits();
		}
		return totalCredits;
	}

	/**
	 * Returns the student's GPA
	 *
	 * @return the GPA that belongs to the student
	 */
	public double getGpa() {
		int multiplier;
		double sum = 0;

		for (int i = 0; i < numberOfCourses; i++) {
			switch(coursesEnrolled[i].getGrade()) {
				case 'A':
					multiplier = 4;
					break;
				case 'B':
					multiplier = 3;
					break;
				case 'C':
					multiplier = 2;
					break;
				case 'D':
					multiplier = 1;
					break;
				case 'F':
					multiplier = 0;
					break;
				default:
					System.out.println("Invalid Course Grade");
					continue;
			}
			sum += coursesEnrolled[i].getCredits() * multiplier;
		}
		return sum / getHoursEnrolled();
	}

	/**
	 * Calculates and returns the billing amount
	 *
	 * @param tuitionRate the rate the tuition is being charged for
	 * @return the tuition fees
	 */
	public double billingAmount(double tuitionRate) {
		return tuitionRate * this.getHoursEnrolled();
	}

	/**
	 * Returns the string for the class
	 *
	 * @param tuitionRate the rate the tuition is being charged for by the user
	 * @return a string containing all the student information
	 */
	public String toString(double tuitionRate) {
		String string = "";
		boolean isPaid = this.isTuitionPaid();
		// checks if a GPA is available to output
		if (isPaid) {
			string = String.format("GPA: %.2f\n", this.getGpa());
		}
		// adds the course information
		for (int i = 0; i < this.getNumberOfCourses(); i++) {
			string += this.getCourse(i).toString(isPaid);
		}
		// checks if the course hasn't been paid for
		if (!isPaid)
			string += String.format("\nGrades are being held for not paying the tuition.\nAmount Due: $%.2f", this.billingAmount(tuitionRate));
		return String.format("Name: %s\nID: %s\nNumber of Courses: %s\nHours: %s\n%s", this.getFirstName() + " " +
				this.getLastName(), this.getId(), this.getNumberOfCourses(), this.getHoursEnrolled(), string);
	}
}

