package Laboratories.src.Seven.Grade;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GradeReportingProgram {

	/**
	 * Member variables
	 */
	private Scanner console = new Scanner(System.in);
	private final int MAX_STUDENTS = 10;
	private Student[] students;
	private int noOfStudents;

	/**
	 * Default constructor
	 */
	public GradeReportingProgram() {
		students = new Student[MAX_STUDENTS];
		for(int i = 0; i < students.length; i++) {
			students[i] = new Student();
		}
		noOfStudents = 0;
	}

	/**
	 * Creates an object of the class to deal with non-static variables
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		GradeReportingProgram gradeReportProgram = new GradeReportingProgram();
		gradeReportProgram.start();
	}

	/**
	 * Asks the user for the number of students that are being entered and their course details
	 */
	public void start() {
		// ask the user for input
		System.out.print("Number of students: ");
		while (true) {
			try {
				noOfStudents = console.nextInt();
				// checks that the number is more >= 1
				if (noOfStudents < 1) {
					throw new InputMismatchException();
				}
			} catch (InputMismatchException e) {
				System.out.println("ERROR: Please enter a valid number");
				continue;
			}
			break;
		}
		System.out.print("Tuition rate: ");
		double tuitionRate;
		while (true) {
			try {
				tuitionRate = console.nextDouble();
				// checks that the number is more >= 1
				if (tuitionRate < 1) {
					throw new InputMismatchException();
				}
			} catch (InputMismatchException e) {
				System.out.println("ERROR: Please enter a valid tuition rate");
				continue;
			}
			break;
		}
		this.getStudentData();
		int choice;
		while (true) {
			System.out.println("Student Number: ");
			try {
				choice = console.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("ERROR: Please enter a valid student number");
				continue;
			}
			if (choice == -1) {
				break;
			}
			System.out.println(students[choice - 1].toString(tuitionRate));
		}
	}

	/**
	 * Requests the user to input data about a student and their courses
	 */
	public void getStudentData() {
		Course[] courses = new Course[6];
		for (int i = 0; i < noOfStudents; i++) {
			System.out.print("First name: ");
			String firstName = console.next();
			System.out.print("Last name: ");
			String lastName = console.next();
			System.out.print("ID: ");
			int id = console.nextInt();
			System.out.print("Tuition paid? (Y/N) ");
			char isPaid = console.next().charAt(0);
			boolean isTuitionPaid = isPaid == 'Y';
			System.out.print("Number of courses: ");
			int noOfCourses = console.nextInt();
			for (int j = 0; j < noOfCourses; j++) {
				System.out.print("Course name: ");
				String courseName = console.next();
				System.out.print("Course number: ");
				String courseNumber = console.next();
				System.out.print("Credits: ");
				int credits = console.nextInt();
				System.out.print("Grade: ");
				char grade = console.next().charAt(0);
				courses[j] = new Course(courseName, courseNumber, grade, credits);
			}
			// creates a new student in the current location
			students[i] = new Student(firstName, lastName, id, noOfCourses, isTuitionPaid, courses);
		}
	}

}

