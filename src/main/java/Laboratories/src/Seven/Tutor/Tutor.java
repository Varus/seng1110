/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Seven.Tutor;

/**
 * A simple class that stores some information that describes a tutor.
 */
public class Tutor {

	/**
	 * Private member variables.
	 */
	private String name;		// The name of the tutor.
	private int grade;			// What grade the tutor is in.
	private int contactHours;	// The contact hours for the tutor.

	/**
	 * @constructor
	 * The default constructor that initializes the default values of the member variables.
	 */
	public Tutor() {
		this.name = "";
		this.grade = 0;
		this.contactHours = 0;
	}

	/**
	 * @constructor
	 * A constructor that sets the name, grade and contact hours of the tutor.
	 *
	 * @param name The name of the tutor.
	 * @param grade The grade of the tutor.
	 * @param contactHours The contact hours of the tutor.
	 */
	public Tutor(String name, int grade, int contactHours) {
		this.name = name;
		this.grade = grade;
		this.contactHours = contactHours;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
