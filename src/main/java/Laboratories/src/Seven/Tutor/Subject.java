/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Seven.Tutor;

import java.util.ArrayList;
import java.util.List;

/**
 * An interface for creating tutors and displaying their information.
 */
public class Subject {

	/**
	 * Private member variables.
	 */
	private List<Tutor> tutors = new ArrayList<>();

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		Subject subject = new Subject();
		subject.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	private void start(String[] args) {
		// Add the tutors to the list
		tutors.add(this.createTutor("Cesar", 2, 8));
		tutors.add(this.createTutor("Joshua", 2, 8));
		tutors.add(this.createTutor("Joe", 2, 8));
		tutors.add(this.createTutor("Michael", 2, 8));
		// Output the tutors
		tutors.forEach(t -> System.out.printf("Tutor %d: %s\n", tutors.indexOf(t) + 1, t.toString()));
	}

	/**
	 * Creates and returns a tutor.
	 *
	 * @param name The name of the tutor.
	 * @param grade The grade of the tutor.
	 * @param contactHours The contact hours of the tutor.
	 * @return The tutor.
	 */
	private Tutor createTutor(String name, int grade, int contactHours) {
		return new Tutor(name, grade, contactHours);
	}

}
