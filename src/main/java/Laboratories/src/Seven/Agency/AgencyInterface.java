/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Seven.Agency;

import Laboratories.src.Seven.Agency.Person.Sex;

import java.util.*;

/**
 * An interface that enables users to create heterosexual couples, test and display them and various data.
 */
public class AgencyInterface {

	/**
	 * Private member variables.
	 */
	private Scanner scanner = new Scanner(System.in);
	private List<Couple> couples = new ArrayList<>();

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		AgencyInterface agencyInterface = new AgencyInterface();
		agencyInterface.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start(String[] args) {
		// Instantiate the option, lower and upper bounds
		int option = 0;
		int lowerBound = 1;
		int upperBound = 7;
		// Loop until the user exits
		do {
			// Display the menu, query the user to pick a menu option and evaluate it
			this.displayMenu();
		    option = this.getMenuOption(lowerBound, upperBound);
			switch (option) {
				case 1:
					this.addCouple();
					break;
				case 2:
					this.testCouple();
					break;
				case 3:
					this.displayCouples();
					break;
				case 4:
					this.getAverageAge();
					break;
				case 5:
					this.maxDifference();
					break;
				case 6:
					this.averageAgeDifference();
					break;
			}
		} while (option != upperBound);
	}

	/**
	 * Displays the menu options.
	 */
	private void displayMenu() {
		System.out.printf("Please choose an option: \n\n\t" +
				"1. Add a new couple\n\t" +
				"2. Test a couple\n\t" +
				"3. Display couples\n\t" +
				"4. Display average age of each gender\n\t" +
				"5. Display the couple with the maximum age difference\n\t" +
				"6. Display the average age difference\n\t" +
				"7. Exit\n\n");
	}

	/**
	 * Displays the message for empty couples.
	 */
	private void displayEmptyCouples() {
		System.out.println("No couples available.");
	}

	/**
	 * This method retrieves a valid menu option from the user.
	 *
	 * @param lowerBound the minimum value for the menu option
	 * @param upperBound the maximum value for the menu option
	 * @return the menu option chosen by the user
	 */
	private int getMenuOption(int lowerBound, int upperBound) {
		if (upperBound < lowerBound) {
			throw new RuntimeException("Bad function call: getMenuOption(). Upper bound must be greater than lower bound.");
		}
		while (true) {
			try {
				int i = scanner.nextInt();
				if (i < lowerBound || i > upperBound) {
					throw new InputMismatchException(String.format("The input you have entered is not between %d and %d.", lowerBound, upperBound));
				}
				return i;
			} catch(InputMismatchException e) {
				String message = e.getMessage();
				System.out.println(message == null ? "Please enter an integer value." : message);
				scanner.nextLine();
			}
		}
	}

	/**
	 * Adds a couple to the list.
	 */
	private void addCouple() {
		couples.add(this.createCouple());
	}

	/**
	 * Creates a new couple and returns it.
	 *
	 * @return The newly added couple.
	 */
	private Couple createCouple() {
		// Create a female and male person
		Person female = this.createPerson(Sex.FEMALE);
		Person male = this.createPerson(Sex.MALE);
		// Create the couple and return it
		return new Couple(female, male);
	}

	/**
	 * Creates and returns a person of a particular sex.
	 *
	 * @param sex The gender of the person being created.
	 * @return The person object.
	 */
	private Person createPerson(Sex sex) {
		// Query the user for the name of the person and store the value
		System.out.printf("%s name: ", sex == Sex.MALE ? "His" : "Her");
		String name = scanner.next();
		// Query the user for the age of the person and store the value
		System.out.print("Age: ");
		int age = scanner.nextInt();
		// Create and return the person object
		return new Person(name, sex, age);
	}

	/**
	 * Tests a couple as specified by the user.
	 */
	private void testCouple() {
		if (couples.isEmpty()) {
			this.displayEmptyCouples();
		} else {
			// Query the user to input which couple they would like to test and display their options
			System.out.println("Which couple would you like to test? ");
			this.displayCouples();
			int index = this.getMenuOption(1, couples.size());
			// Get the selected couple and the members within it
			Couple couple = couples.get(index - 1);
			Person she = couple.getShe();
			Person he = couple.getHe();
			// Test the couple
			System.out.printf("GOOD FOR %s!\n", she.getAge() < he.getAge() ? he.getName() : she.getName());
		}
	}

	/**
	 * A method that displays all the couples.
	 */
	private void displayCouples() {
		if (couples.isEmpty()) {
			this.displayEmptyCouples();
		} else {
			couples.forEach(c -> System.out.printf(String.format("Couple %d:\n%s", couples.indexOf(c) + 1, c.toString())));
		}
	}

	/**
	 * A method that calculates and display the average age of each gender.
	 */
	private void getAverageAge() {
		if (couples.isEmpty()) {
			this.displayEmptyCouples();
		} else {
			// Get the average female and male age and then output it
			double female = couples.stream().mapToInt(c -> c.getShe().getAge()).average().getAsDouble();
			double male = couples.stream().mapToInt(c -> c.getHe().getAge()).average().getAsDouble();
			System.out.printf("Average age for females is %.2f\nAverage age for males is %.2f\n", female, male);
		}
	}

	/**
	 * A method to display the couple with the largest age difference
	 */
	private void maxDifference() {
		if (couples.isEmpty()) {
			this.displayEmptyCouples();
		} else {
			// Get the couple with the largest age difference and output it
			Couple couple = couples.stream().max((c1, c2) -> Integer.compare(
				Math.abs(c1.getShe().getAge() - c1.getHe().getAge()), Math.abs(c2.getShe().getAge() - c2.getHe().getAge())
			)).get();
			System.out.println(couple.toString());
		}
	}

	/**
	 * A method to calculate and display the average age difference between all couples.
	 */
	private void averageAgeDifference() {
		if (couples.isEmpty()) {
			this.displayEmptyCouples();
		} else {
			// Get the average age difference and output it
			double average = couples.stream().mapToInt(c -> Math.abs(c.getShe().getAge() - c.getHe().getAge())).average().getAsDouble();
			System.out.printf("Average age difference is %.1f\n", average);
		}
	}

}

