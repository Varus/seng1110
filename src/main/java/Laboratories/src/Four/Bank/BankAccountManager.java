/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Four.Bank;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A Bank Account Manager class that handles the creation of a Bank Account and its management.
 */
public class BankAccountManager {

	/**
	 * Private member variables.
	 */
	private Scanner scanner = new Scanner(System.in);

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		BankAccountManager bankAccountManager = new BankAccountManager();
		bankAccountManager.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start (String[] args) {
		// Instantiate the menu option and their bounds
		int option = 0;
		int lowerBound = 1;
		int upperBound = 4;
		// Create a bank account based on the account name
		System.out.print("What is the account name? ");
		BankAccount bankAccount = new BankAccount(scanner.nextLine());
		// Loop until the user exits
		do {
			// Display the menu
			this.displayMenu();
			// Ensure the user selects a valid menu option and then evaluate it
			switch (option = this.getMenuOption(lowerBound, upperBound)) {
				case 1:
					this.viewBalance(bankAccount);
					break;
				case 2:
					this.deposit(bankAccount);
					break;
				case 3:
					this.withdraw(bankAccount);
					break;
			}
		} while (option != upperBound);
	}

	/**
	 * Displays the menu options for the user.
	 */
	private void displayMenu() {
		System.out.printf("Please choose an option:\n\n" +
			"\t1. View Balance\n" +
			"\t2. Deposit\n" +
			"\t3. Withdrawal\n" +
			"\t4. Exit\n\n");
	}

	/**
	 * This method retrieves a valid menu option from the user.
	 *
	 * @param lowerBound the minimum value for the menu option
	 * @param upperBound the maximum value for the menu option
	 * @return the menu option chosen by the user
	 */
	private int getMenuOption(int lowerBound, int upperBound) {
		if (upperBound <= lowerBound) {
			throw new RuntimeException("Bad function call: getMenuOption(). Upper bound must be greater than lower bound.");
		}
		while (true) {
			try {
				int i = scanner.nextInt();
				if (i < lowerBound || i > upperBound) {
					throw new InputMismatchException(String.format("The input you have entered is not between %d and %d.", lowerBound, upperBound));
				}
				return i;
			} catch(InputMismatchException e) {
				String message = e.getMessage();
				System.out.println(message == null ? "Please enter an integer value." : message);
				scanner.nextLine();
			}
		}
	}

	/**
	 * Views the balance of a particular account by outputting its details.
	 *
	 * @param bankAccount The bank account having the balance checked.
	 */
	private void viewBalance(BankAccount bankAccount) {
		System.out.printf("The balance in %s is $%s\n", bankAccount.getName(), bankAccount.getBalance());
	}

	/**
	 * A method that is called when the user chooses to deposit money into their bank account.
	 *
	 * @param bankAccount The bank account that will have money deposited.
	 */
	private void deposit(BankAccount bankAccount) {
		System.out.print("Amount to deposit: ");
		this.makeDeposit(bankAccount, scanner.nextDouble());
	}

	/**
	 * Deposits a certain amount into the specified bank account.
	 *
	 * @param bankAccount The bank account that is having money deposited.
	 * @param amount The amount to deposit into the bank account.
	 */
	private void makeDeposit(BankAccount bankAccount, double amount) {
		bankAccount.deposit(amount);
	}

	/**
	 * A method that is called when the user chooses to withdraw money from their bank account.
	 *
	 * @param bankAccount The bank account that will have money withdrawn.
	 */
	private void withdraw(BankAccount bankAccount) {
		System.out.print("Amount to withdraw: ");
		this.makeWithdrawal(bankAccount, scanner.nextDouble());
	}

	/**
	 * Withdraws a certain amount from the specified bank account.
	 *
	 * @param bankAccount The bank account that is being withdrawn from.
	 * @param amount The amount to withdraw from the bank account.
	 */
	private void makeWithdrawal(BankAccount bankAccount, double amount) {
		try {
			bankAccount.withdraw(amount);
		} catch (BankAccountException e) {
			System.out.println(e.getMessage());
		}
	}

}
