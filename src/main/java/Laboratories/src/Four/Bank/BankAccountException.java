/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Four.Bank;

/**
 * A custom bank account exception.
 */
public class BankAccountException extends Exception {
    public BankAccountException(String message) {
        super(message);
    }
}
