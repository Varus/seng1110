/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Four.Bank;

/**
 * A Bank Account class that enables deposits and withdrawals.
 */
public class BankAccount {

	/**
	 * Private member variables.
	 */
	private String id;		// The id of the bank account.
	private String name;	// The name of the bank account.
	private double balance; // The current balank of the bank account.

	/**
	 * Constructs the default bank account.
	 *
	 * @constructor
	 */
	public BankAccount() {
		this.id = "NO ID NUMBER";
		this.name = "NO NAME";
		this.balance = 0;
	}

	/**
	 * Constructs a bank account with the specified name.
	 *
	 * @constructor
	 * @param name The name of the account.
	 */
	public BankAccount(String name) {
		this();
		this.name = name;
	}

	/**
	 * Deposits a specified amount into the bank account.
	 *
	 * @param amount The amount to deposit from the account.
	 */
	public void deposit(double amount) {
		balance += amount;
	}

	/**
	 * Withdraws a specified amount from the bank account.
	 *
	 * @param amount The amount to widthdraw from the account.
	 */
	public void withdraw(double amount) throws BankAccountException {
		if (amount > balance) {
			throw new BankAccountException("Cannot withdraw an amount more than your current balance.");
		}
		balance -= amount;
	}

	/**
	 * A mutator method that sets the name of the account.
	 *
	 * @param name The new name of the account.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the account.
	 *
	 * @return The name of the account.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * An accessor method that returns the balance of the account.
	 *
	 * @return The current balance of the account.
	 */
	public double getBalance() {
		return this.balance;
	}

}
