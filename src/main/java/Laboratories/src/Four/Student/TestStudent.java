/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Four.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * A class that tests that the Student class functions by creating a specified amount of students and computing their
 * highest test result and their average mark.
 */
public class TestStudent {

	/**
	 * Private member variables.
	 */
	private List<Student> students = new ArrayList<>();

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate the class and begin the program
		TestStudent testStudent = new TestStudent();
		testStudent.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start (String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Query the user for how many students are being created
		System.out.print("How many students are being examined? ");
		int length = scanner.nextInt();
		// Iterate through every student and create each
		for (int i = 1; i <= length; i++) {
			// Adds the student to the list
			students.add(this.createStudent(scanner));
		}
		students.forEach(System.out::println);
	}

	/**
	 * Creates and returns the student object and stores their test results.
	 *
	 * @param scanner The input scanner.
	 * @return Student The student that was created.
	 */
	public Student createStudent(Scanner scanner) {
		// Flush the input
		scanner.nextLine();
		// Queries the user for the student name and then creates the Student
		System.out.print("Name: ");
		String name = scanner.nextLine();
		Student student = new Student(name);
		// Queries the user for how many test results are being stored and records the value
		System.out.printf("How many test results are being recorded for %s? ", name);
		int results = scanner.nextInt();
		// Iterates through every result being added
		for (int i = 1; i <= results; i++) {
			// Queries the user for the test result and adds it
			System.out.printf("Mark for test %d: ", i);
			student.addResult(scanner.nextInt());
		}
		return student;
	}

}
