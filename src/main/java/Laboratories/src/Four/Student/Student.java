/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Four.Student;

import java.util.ArrayList;

/**
 * A simple Student class that stores their name and test results. This class is able to retrieve their average mark
 * and their high score of all test results.
 */
public class Student {

	/**
	 * Private Member variables
	 */
	private String name;				// The name of the student.
	private ArrayList<Integer> results;	// The test results of the student.

	/**
	 * @constructor
	 * The default student constructor that initialises the private member variables.
	 */
	public Student() {
		this.name = "";
		this.results = new ArrayList<>();
	}

	/**
	 * @constructor
	 * The student constructor that initialises their name.
	 *
	 * @param name The name of the student.
	 */
	public Student(String name) {
		this();
		this.name = name;
	}

	/**
	 * A mutator method that sets the name of the student.
	 *
	 * @param name The name of the student.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the student.
	 *
	 * @return The name of the student.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * A method that adds a result to the student.
	 *
	 * @param result The result of the current test.
	 */
	public void addResult(int result) {
		results.add(result);
	}

	/**
	 * An accessor method that returns the current test result.
	 *
	 * @param index The index of the result.
	 * @return The value of the test result.
	 */
	public int getScore(int index) {
		return results.get(index);
	}

	/**
	 * Calculates and returns the average score of all test results.
	 *
	 * @return The average of the student's test results.
	 */
	public double getAverage() {
		return results.stream().mapToInt(r -> r).average().getAsDouble();
	}

	/**
	 * Calculates and returns the student's highest test result.
	 *
	 * @return The student's highest test result.
	 */
	public int getHighScore() {
		return results.stream().max(Integer::compare).get();
	}

	@Override
	public String toString() {
		return String.format("%s has an average of %.2f and their high score is %d.",
			this.getName(),
			this.getAverage(),
			this.getHighScore()
		);
	}

}  




