package Laboratories.src.Three;

import java.util.Scanner;

public class Salary {

	/**
	 * Calculates the salary based on a given number of weeks and hours worked each week
	 *
	 * @param args command line arguments
	 */
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		String name = "";
		double hours;
		double overtime;
		double total = 0;
		double bonus = 0;
		int weeks = 0;
		final double normalRate = 10;
		final double extraRate = 15;
		final int overtimeHours = 40;
		// asks the user for their name
		System.out.print("What is your name: ");
		name = scanner.next();
		// loops until a valid week has been entered
		do {
			System.out.print("How many weeks have you worked: ");
			weeks = scanner.nextInt();
			// checks for invalid input
			if (weeks <= 0) {
				System.out.print("Please input a positive value");
			}
		} while (weeks <= 0);
		// loops through every week asking the user for their hours worked
		for (int i = 0; i < weeks; i++) {
			// asks the user the hours they worked for the current week
			System.out.printf("Please enter number of hours worked in week %s: ", i + 1);
			hours = scanner.nextInt();
			// checks for invalid input
			if (hours < 0) {
				System.out.print("Invalid input");
				System.exit(1);
			}
			// calculates the hours worked overtime
			overtime = hours > overtimeHours ? hours - overtimeHours : 0;
			// calculates the total salary
			total += (hours - overtime) * normalRate + overtime * extraRate;
		}
		// calculates the bonus
		if (total <= 1000) {
			// 0 - 1000
			total *= 1.1;
			bonus = 10;
		} else if (total > 1000 && total <= 2000) {
			// 1001 - 2000
			total *= 1.05;
			bonus = 5;
		} else {
			if (total > 2000 && total <= 3000) {
				// 2001 - 3000
				total *= 1.01;
				bonus = 1;
			}
		}
		// outputs the total salary
		System.out.printf("Salary of %s is $%.2f%s\n", name, total, bonus == 0 ? "" : " which includes a " + bonus + "% bonus");
	}

}
