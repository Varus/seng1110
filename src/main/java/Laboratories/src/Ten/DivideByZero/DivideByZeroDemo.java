/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Ten.DivideByZero;

import java.util.Scanner;

/**
 * A simple Division class that asks the user for two values and then divides them. If the denominator is zero then the
 * program will throw a custom exception and ask the user to input the denominator again.
 */
public class DivideByZeroDemo {

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(String [] args) {
		DivideByZeroDemo divideByZeroDemo = new DivideByZeroDemo();
		divideByZeroDemo.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start(String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Query the user to input the numerator and store its value
		System.out.print("Enter the numerator: ");
		double numerator = scanner.nextDouble();
		// Loops until the program has been run successfully
		while (true) {
			try {
				// Query the user to input the denominator and store its value
				System.out.print("Enter the denominator: ");
				double denominator = scanner.nextDouble();
				// Check if the denominator is zero
				if (denominator == 0) {
					// Throw the exception
					throw new DivideByZeroException();
				}
				// Output the division
				System.out.printf("%s / %s = %.2f\n", numerator, denominator, numerator / denominator);
			} catch (DivideByZeroException e) {
				// Output the error message and rerun the loop
				System.out.println(e.getMessage());
				continue;
			}
			// Escape the loop
			break;
		}
	}

}
