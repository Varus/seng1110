/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Ten.DivideByZero;

/**
 * A custom divide by zero exception.
 */
public class DivideByZeroException extends Exception {

	/**
	 * @constructor
	 * The default constructor that calls the super class with an error message.
	 */
	public DivideByZeroException() {
		super("You cannot divide by zero.");
	}

	/**
	 * @constructor
	 * A constructor that stores the specified error message.
	 *
	 * @param message The error message that details the exception.
	 */
	public DivideByZeroException(String message) {
		super(message);
	}

}