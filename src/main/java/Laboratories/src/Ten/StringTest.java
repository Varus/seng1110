/**
 * @author Monica Olejniczak
 */
package Laboratories.src.Ten;

import java.util.Scanner;

/**
 * A class that reverses a string using recursion.
 */
public class StringTest {

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {
		StringTest stringTest = new StringTest();
		stringTest.start(args);
	}

	/**
	 * The method called by the main entry point.
	 *
	 * @param args The command line arguments.
	 */
	public void start(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please enter a string to reverse: ");
		System.out.println(this.reverse(scanner.next()));
	}

	/**
	 * Reverses a string using recursion.
	 *
	 * @param string The string being reversed.
	 * @return The reversed string.
	 */
	public String reverse(String string) {
		// Checks if the reverse method is finished
		if (string.length() <= 1) {
			// Return the string
			return string;
		} else {
			// Call the method again taking everything but the first character and append it to the end
			return reverse(string.substring(1)) + string.charAt(0);
		}
	}

}
