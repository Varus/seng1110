/**
 * @author Monica Olejniczak
 */
package Laboratories.src.One;

import java.util.Scanner;

/**
 * A simple Salary class that asks the user for their normal and extra hours and then calculates their total pay.
 */
public class Salary {

	/**
	 * Private member variables to hold the base and extra rate.
	 */
	private static final double NORMAL_RATE = 10;
	private static final double EXTRA_RATE = 15;

	/**
	 * The main entry point of the program.
	 *
	 * @param args The command line arguments.
	 */
	public static void main (String[] args) {
		// Instantiate an input scanner
		Scanner scanner = new Scanner(System.in);
		// Enable the user to input their normal hours and then store the value
		System.out.print("Please enter number of normal hours: ");
		double normal = scanner.nextDouble();
		// Enable the user to input their extra hours and then store the value
		System.out.print("Please enter number of extra hours: ");
		double extra = scanner.nextDouble();
		// Calculate the total salary and display the value
		double total = normal * NORMAL_RATE + extra * EXTRA_RATE;
		System.out.printf("Total salary is: $%.2f\n", total);
	}

}
