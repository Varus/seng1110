package Assignments.src.MatesCoffee.Two;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MatesInterface {

	/**
	 * Member variables
	 */
	private Scanner scanner;
	private Store[] stores;

	/**
	 * Default constructor
	 */
	public MatesInterface() {
		scanner = new Scanner(System.in);
		stores = new Store[2];
		stores[0] = new Store("Lambton");
		stores[1] = new Store("Callaghan");
	}

	/**
	 * The main method which creates a new object of the class to call the start method
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		MatesInterface matesInterface = new MatesInterface();
		matesInterface.mainMenu();
	}

	private void mainMenu() {
		// catches any other errors such as terminating the program early, exits with error code
		try {
			int option;
			// exits the program if 5 is entered
			do {
				// displays the menu
				System.out.printf("Welcome to Mate's Coffee Replenishment System!\n" +
						"Please choose a menu option:\n\n\t" +
						"1. Choose store\n\t" +
						"2. Display stores\n\t" +
						"3. Open\n\t" +
						"4. Save\n\t" +
						"5. Exit\n\n");
				// goes through the selected input
				switch (option = this.getMenuOption(1, 5)) {
					case 1:
						this.chooseStore();
						break;
					case 2:
						this.displayStores();
						break;
					case 3:
						this.open();
						break;
					case 4:
						this.save();
						break;
					case 5:
						break;
					default:
						this.invalidMenuMessage();
						break;
				}
			} while (option != 5);
		} catch (Exception e) {
			System.out.println("ERROR: An unexpected error occurred");
		}
	}

	/**
	 * Allows the user to choose a store
	 */
	private void chooseStore() {
		System.out.printf("Stores: \n%s\nPlease select a store: ", this.getStoreNames());
		Store currentStore = this.getStore(scanner.next());
		while (currentStore == null) {
			System.out.println("ERROR: This is not a valid store name. Please try again.");
			currentStore = this.getStore(scanner.next());
		}
		this.storeMenu(currentStore);
	}

	/**
	 * A method to return the store names in the array
	 *
	 * @return the available store names
	 */
	private String getStoreNames() {
		String output = "";
		for (Store store : stores) {
			output += String.format("\t%s\n", store.getName());
		}
		return output;
	}

	/**
	 * Attempts to get the store based on the name parameter
	 *
	 * @param name the name of the store
	 * @return the store that matches the name
	 */
	private Store getStore(String name) {
		for (Store store : stores) {
			// checks if the store name is the inputted name
			if (store.getName().equalsIgnoreCase(name)) {
				// the store has been found
				return store;
			}
		}
		// the store has not been found
		return null;
	}

	/**
	 * Displays the store menu
	 *
	 * @param currentStore the store in selection
	 */
	private void storeMenu(Store currentStore) {
		int option = 0;
		while (option != 5) {
			System.out.printf("You are currently viewing the %s store\n" +
					"Please choose a menu option:\n\n\t" +
					"1. Add/Edit product\n\t" +
					"2. Delete product\n\t" +
					"3. Display product\n\t" +
					"4. Display all products\n\t" +
					"5. Exit\n\n", currentStore.getName());
			switch (option = this.getMenuOption(1, 5)) {
				case 1:
					this.addProduct(currentStore);
					break;
				case 2:
					this.deleteProduct(currentStore);
					break;
				case 3:
					this.displayProduct(currentStore);
					break;
				case 4:
					this.displayAllProducts(currentStore);
					break;
				case 5:
					break;
				default:
					this.invalidMenuMessage();
					break;
			}
		}
	}

	/**
	 * Adds a new product to the selected store
	 *
	 * @param currentStore the store in selection
	 */
	private void addProduct(Store currentStore) {
		// ask the user for a product name
		System.out.print("Please enter the name of the product: ");
		String name = scanner.next().toLowerCase();
		// checks if the product is taken
		if (currentStore.isProductTaken(name)) {
			this.existingProductMenu(currentStore, name);
		} else {
			currentStore.addProduct(this.requestProduct(currentStore, name));
			System.out.printf("MESSAGE: The product %s has been added to the %s store.\n\n", name, currentStore.getName());
		}
	}

	/**
	 * A menu that is displayed when the user attempts to add a product that already exists in the store
	 *
	 * @param currentStore the store in selection
	 * @param name the name of the product being modified
	 */
	private void existingProductMenu(Store currentStore, String name) {
		// displays the existing product options
		System.out.printf("This product already exists. Please select one of the following:\n\n\t" +
				"1. Change the name of the product\n\t" +
				"2. Change the data in this product\n\n");
		// asks the user for an integer input
		switch (this.getMenuOption(1, 2)) {
			case 1:
				this.addProduct(currentStore);
				break;
			case 2:
				currentStore.editProduct(currentStore.getProductIndex(name), this.requestProduct(currentStore, name));
				System.out.printf("MESSAGE: The product %s has been edited.\n\n", name);
				break;
			default:
				this.invalidMenuMessage();
				break;
		}
	}
	
	/**
	 * Requests the product information from the user
	 *
	 * @param currentStore the store in selection
	 * @param name the name of the product information being requested
	 */
	private Product requestProduct(Store currentStore, String name) {
		while (true) {
			// receive inputs from the user
			int demandRate = (int) Math.ceil(this.getProductInput("demand rate"));
			double setupCost = this.getProductInput("setup cost");
			double unitCost = this.getProductInput("unit cost");
			double inventoryCost = this.getProductInput("inventory cost");
			double sellingPrice = this.getProductInput("selling price");
			// checks if the eoq is feasible
			if (currentStore.getOrderQuantity(demandRate, setupCost, inventoryCost) < demandRate) {
				System.out.println("ERROR: The economic order quantity is not feasible. It is not possible to have a replacement strategy. Please try again");
				continue;
			}
			return new Product(name, demandRate, setupCost, unitCost, inventoryCost, sellingPrice);
		}
	}

	/**
	 * Deletes a product as specified by the user
	 *
	 * @param currentStore the store in selection
	 */
	private void deleteProduct(Store currentStore) {
		// checks if the store is empty
		if (currentStore.isEmpty()) {
			this.emptyStoreMessage();
		} else {
			// ask the user for a product name
			System.out.print("Please enter the name of the product you would like to delete: ");
			String name = scanner.next();
			// check if the product exists in the store
			if (currentStore.isProductTaken(name)) {
				// deletes the product
				currentStore.deleteProduct(currentStore.getProductIndex(name));
				System.out.printf("The product %s was deleted from %s.\n\n", name, currentStore.getName());

			} else {
				this.noProductsMessage();
			}
		}
	}

	/**
	 * Displays data about a specific product and its replenishment strategy
	 *
	 * @param currentStore the store in selection
	 */
	private void displayProduct(Store currentStore) {
		// check if the store is empty
		if (currentStore.isEmpty()) {
			this.emptyStoreMessage();
		} else {
			// ask the user for a product name
			System.out.print("Please enter the name of the product: ");
			String name = scanner.next();
			// check if the product exists in the store
			if (currentStore.isProductTaken(name)) {
				System.out.printf("\n%s\n", currentStore.getProduct(name).toString());
				// enter the replenishment strategy menu
				this.replenishmentStrategyMenu(currentStore, name);
			} else {
				this.noProductsMessage();
			}
		}
	}

	/**
	 * Asks the user if they would like to see the replenishment strategy
	 *
	 * @param currentStore the store in selection
	 * @param name the name of the product
	 */
	private void replenishmentStrategyMenu(Store currentStore, String name) {
		// displays the menu option
		System.out.printf("Would you like to view the replenishment strategy:\n\n\t" +
				"1. Yes\n\t" +
				"2. No\n\n");
		// asks the user for an integer input
		switch (this.getMenuOption(1, 2)) {
			case 1:
				this.replenishmentStrategy(currentStore, name);
				break;
			case 2:
				break;
			default:
				this.invalidMenuMessage();
				break;
		}
	}

	/**
	 * Displays the replenishment strategy for a particular product
	 *
	 * @param currentStore the store in selection
	 * @param name the name of the product
	 */
	private void replenishmentStrategy(Store currentStore, String name) {
		System.out.println("Please enter the number of weeks: ");
		int weeks = this.getInteger(1, Integer.MAX_VALUE);
		Product product = currentStore.getProduct(name);
		List<Integer> list = currentStore.getReplenishmentStrategy(product, weeks);
		String string = "%10s%20s%12s%12s\n";
		int ordersMade = 0;
		int purchaseAmount = 0;
		int inventoryTotal = 0;
		System.out.printf(string, "Weeks", "Quantity Order", "Demand", "Inventory");
		// loops through the list and outputs each element
		for (int i = 0; i < list.size(); i += 4) {
			// checks if an order has been made within the list
			if (list.get(i + 1) > 0) {
				ordersMade++;
				purchaseAmount += list.get(i + 1);
			}
			inventoryTotal += list.get(i + 3);
			System.out.printf(string, list.get(i), list.get(i + 1), list.get(i + 2), list.get(i + 3));
		}
		double purchaseCost = ordersMade * product.getSetupCost() + purchaseAmount * product.getUnitCost();
		double totalInventoryCost = inventoryTotal * product.getInventoryCost();
		double profit = product.getDemandRate() * weeks * product.getSellingPrice() - (purchaseCost + totalInventoryCost);
		System.out.printf("\nPurchase Cost: $%.2f\nInventory Cost: $%.2f\nTotal Cost: $%.2f\n" +
				"Profit: $%.2f\n\n", purchaseCost, totalInventoryCost,
				purchaseCost + totalInventoryCost, profit);
	}

	/**
	 * Displays all the product data in the current store
	 *
	 * @param currentStore the store in selection
	 */
	private void displayAllProducts(Store currentStore) {
		// checks if the store is empty
		if (currentStore.isEmpty()) {
			this.emptyStoreMessage();
		} else {
			// displays the products in the current store
			System.out.print(currentStore.getProductData());
		}
	}

	/**
	 * A generic message for an empty store
	 */
	private void emptyStoreMessage() {
		System.out.println("MESSAGE: There are no products in the store.\n");
	}

	/**
	 * A generic message for a product that does not exist
	 */
	private void noProductsMessage() {
		System.out.println("ERROR: The product does not exist.\n");
	}

	/**
	 * A generic message for an invalid menu option
	 */
	private void invalidMenuMessage() {
		System.out.println("ERROR: This should never execute.\n");
	}

	/**
	 * Displays the contents of each store
	 */
	private void displayStores() {
		for (Store store : stores) {
			System.out.println(store.toString());
		}
	}

	/**
	 * Opens a specified file by loading its data into the stores array
	 */
	private void open() {
		while (true) {
			System.out.print("Please input the name of the file: ");
			String file = scanner.next();
			try {
				Scanner scanner = new Scanner(new File(file));
				Store store = new Store();
				// loop for every line
				while (scanner.hasNextLine()) {
					String[] input = scanner.nextLine().split(": ");
					// check for a blank line
					if (!input[0].equals("")) {
						// check if at a potential store by removing the semicolon
						Store temp = this.getStore(input[0].substring(0, input[0].length() - 1));
						if (temp != null) {
							// get the next store and remove its current products
							store = temp;
							store.removeProducts();
						} else {
							// check for a product
							if (input[0].equalsIgnoreCase("name")) {
								String name = input[1];
								// assume values are loaded correctly from the file
								int demandRate = Integer.parseInt(this.getProductValue(scanner));
								double setupCost = Double.parseDouble(this.getProductValue(scanner));
								double unitCost = Double.parseDouble(this.getProductValue(scanner));
								double inventoryCost = Double.parseDouble(this.getProductValue(scanner));
								double sellingPrice = Double.parseDouble(this.getProductValue(scanner));
								store.addProduct(new Product(name, demandRate, setupCost, unitCost, inventoryCost, sellingPrice));
							}
						}
					}
				}
				// close the scanner
				scanner.close();
				break;
			} catch (FileNotFoundException e) {
				System.out.println("ERROR: The file could not be found. Please try again.\n");
			} catch (Exception e) {
				System.out.println("ERROR: There are errors in the specified file and it could not be read.\n");
				return;
			}
		}
		System.out.println("MESSAGE: The file was loaded successfully.\n");
	}

	/**
	 * Acquires the product value when loading a file
	 *
	 * @param scanner the data file source
	 * @return the respective product value
	 */
	private String getProductValue(Scanner scanner) {
		return scanner.nextLine().split(": ")[1];
	}

	/**
	 * Saves the data in the stores to a specified file
	 */
	private void save() {
		System.out.print("Please input the name of the file: ");
		try {
			PrintWriter writer = new PrintWriter(scanner.next());
			// loop through every store
			for (Store store : stores) {
				writer.printf("%s:\n\n", store.getName());
				// check if the store has products
				if (!store.isEmpty()) {
					writer.print(store.getProductData());
				}
			}
			// close the writer
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: An error was encountered when writing to the specified file.\n");
		}
		System.out.println("MESSAGE: The file was written to successfully.\n");
	}

	/**
	 * This method asks the user for product input and returns the data once validated
	 *
	 * @param string the string for the current input option
	 * @return a double containing the validating user input
	 */
	private double getProductInput(String string) {
		double input;
		// loops until the demand rate is valid
		while (true) {
			try {
				System.out.printf("Please enter the %s of the product: ", string);
				input = scanner.nextDouble();
				// checks if the input is negative
				if (input < 0) {
					throw new InputMismatchException();
				}
				break;
			} catch (InputMismatchException e) {
				System.out.println("ERROR: The value you entered must be a number and cannot be negative. Please try again.");
				this.flushInput();
			}
		}
		return input;
	}
	
	/**
	 * This method retrieves a valid menu option from the user
	 *
	 * @param lowerBound the minimum value for the menu option
	 * @param upperBound the maximum value for the menu option
	 * @return the menu option chosen by the user
	 */
	private int getMenuOption(int lowerBound, int upperBound) {
		return this.getInteger(lowerBound, upperBound, "ERROR: That is not a valid menu option. Please try again.");
	}
	
	/**
	 * This method retrieves a valid integer from the user
	 *
	 * @param lowerBound the minimum value for the integer
	 * @param upperBound the maximum value for the integer
	 * @return an integer chosen by the user
	 */
	private int getInteger(int lowerBound, int upperBound) {
		return this.getInteger(lowerBound, upperBound, String.format("ERROR: The input you have entered is not between %d and %d.", lowerBound, upperBound));
	}
	
	/**
	 * This method retrieves a valid integer from the user with a specific error message
	 *
	 * @param lowerBound the minimum value for the integer
	 * @param upperBound the maximum value for the integer
	 * @param errorMessage the message upon error
	 * @return the integer chosen by the user
	 */
	private int getInteger(int lowerBound, int upperBound, String errorMessage) {
		if (upperBound <= lowerBound) {
            throw new RuntimeException("ERROR: Bad function call: getInteger(). Upper bound must be greater than lower bound.");
        }
		while (true) {
            try {
				int i = scanner.nextInt();
				if (i < lowerBound || i > upperBound) {
					throw new InputMismatchException();
				}
				return i;
			} catch(InputMismatchException e) {
				System.out.println(errorMessage);
				this.flushInput();
			}
        }
	}

	/**
	 * This flushes the scanner input and is only used during validation
	 */
	private void flushInput() {
		scanner.nextLine();
	}

}
