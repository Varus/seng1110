package Assignments.src.MatesCoffee.Two;

import java.util.*;

public class Store {

	/**
	 * Member variables
	 */
	private String name;
	private final int INITIAL = 3;
	private Product[] products;
	private int productCount;

	/**
	 * Default constructor
	 */
	public Store() {
		products = new Product[INITIAL];
		productCount = 0;
	}

	/**
	 * A constructor that sets the name of the store
	 *
	 * @param name the name of the store
	 */
	public Store(String name) {
		this();
		this.name = name;
	}

	/**
	 * An accessor method that returns the name of the store
	 *
	 * @return the name of the store
	 */
	public String getName() {
		return name;
	}

	/**
	 * Adds a product to the store
	 *
	 * @param product the product that will be added to the store
	 */
	public void addProduct(Product product) {
		if (products.length == productCount) {
			this.resize(productCount + 1);
		}
		products[productCount] = product;
		productCount++;
	}

	/**
	 * Edits the data for a particular product
	 *
	 * @param index the index of the product being modified
	 * @param product the new product information
	 */
	public void editProduct(int index, Product product) {
		products[index] = product;
	}

	public void deleteProduct(int index) {
		System.arraycopy(products, index + 1, products, index, productCount - 1 - index);
		productCount--;
	}

	/**
	 * Removes the products by reinitialising the array
	 */
	public void removeProducts() {
		products = new Product[INITIAL];
		productCount = 0;
	}

	/**
	 * Resizes the array
	 */
	private void resize(int size) {
		products = Arrays.copyOf(products, size);
	}

	/**
	 * Returns the product with the specified name
	 *
	 * @param name the name of the product being checked against
	 * @return the product
	 */
	public Product getProduct(String name) {
		int index = this.getProductIndex(name);
		return index == -1 ? null : products[index];
	}

	/**
	 * Returns the index of the specified product
	 *
	 * @param name the name of the product being checked against
	 * @return the index of the specified product
	 */
	public int getProductIndex(String name) {
		for (int i = 0; i < productCount; i++) {
			if (products[i].getName().equalsIgnoreCase(name)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Checks if the store is empty
	 *
	 * @return whether the store is empty
	 */
	public boolean isEmpty() {
		return productCount == 0;
	}

	/**
	 * Checks if the product exists in the store
	 *
	 * @param name the name of the product being checked against
	 * @return if the product exists in the store
	 */
	public boolean isProductTaken(String name) {
		return getProductIndex(name) > -1;
	}

	/**
	 * Calculates the optimal order quantity for a product using a product
	 *
	 * @param product the product to calculate the order quantity for
	 * @return the product's order quantity
	 */
	public int getOrderQuantity(Product product) {
		return (int)Math.ceil(Math.sqrt(2 * product.getSetupCost() * product.getDemandRate() / product.getInventoryCost()));
	}

	/**
	 * Calculates the optimal order quantity for a product using the parameters necessary
	 *
	 * @param demandRate the demand rate of a product
	 * @param setupCost the setup cost of a product
	 * @param inventoryCost the inventory cost of a product
	 * @return the optimal order quantity
	 */
	public int getOrderQuantity(double demandRate, double setupCost, double inventoryCost) {
		return (int)Math.ceil(Math.sqrt(2 * setupCost * demandRate / inventoryCost));
	}

	/**
	 * Calculates and returns the replenishment strategy given a product
	 *
	 * @param product the product to calculate the replenishment strategy for
	 * @param weeks the weeks for the replenishment strategy
	 * @return a string containing the replenishment strategy
	 */
	public List<Integer> getReplenishmentStrategy(Product product, int weeks) {
		List<Integer> list = new ArrayList<>();
		int orderQuantity = this.getOrderQuantity(product);
		int finalOrder = (int)Math.ceil((double)product.getDemandRate() * weeks / orderQuantity);
		int ordersMade = 1;
		int inventory = orderQuantity;
		// loops through all the weeks
		for (int i = 1; i <= weeks; i++) {
			// resets the eoq and inventory
			if (inventory - product.getDemandRate() < 0) {
				ordersMade++;
				if (ordersMade == finalOrder) {
					orderQuantity -= (inventory - product.getDemandRate() + orderQuantity) - (weeks - i) * product.getDemandRate();
				}
				else {
					orderQuantity = this.getOrderQuantity(product);
				}
				inventory += orderQuantity - product.getDemandRate();
			} else {
				// reduces the inventory
				inventory -= product.getDemandRate();
			}
			list.addAll(Arrays.asList(i, orderQuantity, product.getDemandRate(), inventory));
			orderQuantity = 0;
		}
		return list;
	}

	/**
	 * A method that returns the product data in the store
	 *
	 * @return the output for the products in the store
	 */
	public String getProductData() {
		String output = "";
		for (int i = 0; i < productCount; i++) {
			output += String.format("%s\n", products[i].toString());
		}
		return output;
	}

	/**
	 * An overriden toString method to return the store string
	 *
	 * @return the store string
	 */
	@Override
	public String toString() {
		String output = String.format("Store: %s\n\tNumber of products: %d\n", name, productCount);
		for (int i = 0; i < productCount; i++) {
			output += String.format("\tProduct %d: %s\n", i + 1, products[i].getName());
		}
		return output;
	}

}
