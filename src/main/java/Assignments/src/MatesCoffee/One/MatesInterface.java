package Assignments.src.MatesCoffee.One;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MatesInterface {

	/**
	 * Member variables
	 */
	private Scanner scanner;
	private Store store;

	/**
	 * Default constructor
	 */
	public MatesInterface() {
		scanner = new Scanner(System.in);
		store = new Store();
	}

	/**
	 * The main method which creates a new object of the class to call the start method
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		MatesInterface matesInterface = new MatesInterface();
		matesInterface.start();
	}

	/**
	 * A method that asks the user for a menu option. The user is able to choose from the following options:
	 *
	 *      1. They are able to input data for a product and may store a total of three products in the store
	 *      2. They are able to view the data for a specific product that exists in the store
	 *      3. They are able to request the replenishment strategy for a specific product that exists in the store by inputting the amount of weeks
	 *      4. They are able to exit the program and the product with the highest profit is viewed before exiting
	 *
	 */
	private void start() {
		int input = 0;
		int weeks = 0;
		// catches any other errors such as terminating the program early, exits with error code
		try {
			// exits the program if 4 is entered
			do {
				System.out.printf("Welcome to Mate's Coffee Replenishment System!\n" +
						"Please choose a menu option:\n\n\t" +
						"1. Input data for one product\n\t" +
						"2. Show data from one product\n\t" +
						"3. Show the replenishment strategy for a product\n\t" +
						"4. Exit program\n\n");
				boolean invalid = true;
				// asks the user for a menu option
				while (invalid) {
					try {
						input = scanner.nextInt();
						// error checking the menu option input is within the bounds
						if (input < 0 || input > 4) {
							throw new InputMismatchException();
						}
					} catch (InputMismatchException e) {
						System.out.println("That is not a valid menu option. Please try again.");
						continue;
					}
					invalid = false;
				}
				// goes through the selected input
				switch (input) {
					case 1:
						this.menuOne();
						break;
					case 2:
						this.menuTwo(false);
						break;
					case 3:
						weeks = this.menuTwo(true);
						break;
					default: break;
				}
			} while (input != 4);
		} catch (Exception e) {
			System.out.println("ERROR: An unexpected error occurred");
		}
		// checks for errors
		if (weeks == 0 || !store.isProductData()) {
			// checks if the replenishment strategy menu has been viewed
			if (weeks == 0) {
				System.out.println("ERROR: A replenishment strategy was never viewed.");
			}
			// checks if there are products in the store
			if (!store.isProductData()) {
				System.out.println("ERROR: There are no products in the store and the profit could not be calculated.");
			}
		} else {
			// display product with highest profit as there are no errors
			System.out.printf("Currently displaying the product with the highest profit:\n%s", store.getHighestProfit(weeks).toString());
		}
	}

	/**
	 * This menu adds a new product to the store by asking the user for the relevant product input
	 */
	private void menuOne() {
		String name = "";
		int demandRate = 0;
		double setupCost = 0;
		double unitCost = 0;
		double inventoryCost = 0;
		double sellingPrice = 0;
		// flush the input
		this.flushInput();
		// checks if the store is not yet full of products
		if (!store.isMaxedOut()) {
			boolean productExists = false;
			boolean invalid = true;
			// loops until the name is valid
			while (invalid) {
				// asks the user for input
				System.out.println("Please enter the name of the product: ");
				name = scanner.nextLine();
				// checks the length of the product name
				if (name.length() < 3 || name.length() > 10) {
					System.out.println("ERROR: The product name must be between 3-10 characters. Please try again.");
					continue;
				}
				// product already exists
				if (store.isProductTaken(name)) {
					System.out.printf("This product already exists. Please select one of the following:\n\n\t" +
							"1. Change the name of the product\n\t" +
							"2. Change the data in this product\n\n");
					try {
						// asks the user for an integer input
						switch (scanner.nextInt()) {
							case 1:
								continue;
							case 2:
								productExists = true;
								break;
							default:
								throw new InputMismatchException();
						}
					} catch (InputMismatchException e) {
						System.out.println("ERROR: That is not a valid menu option. Please try again.");
						continue;
					}
				}
				invalid = false;
			}
			invalid = true;
			// ensures the eqo is valid
			while (invalid) {
				demandRate = (int)Math.ceil(this.getInput("demand rate"));
				setupCost = this.getInput("setup cost");
				unitCost = this.getInput("unit cost");
				inventoryCost = this.getInput("inventory cost");
				sellingPrice = this.getInput("selling price");
				// checks if the eoq is feasible
				if (store.getOrderQuantity(demandRate, setupCost, inventoryCost) < demandRate) {
					System.out.println("ERROR: The economic order quantity is not feasible. It is not possible to have a replacement strategy. Please try again");
					continue;
				}
				invalid = false;
			}
			System.out.println(); // new line
			if (productExists) {
				// changes the data in the product if it already exists
				store.setExistingProduct(new Product(name.toLowerCase(), demandRate, setupCost, unitCost, inventoryCost, sellingPrice));
			} else {
				// sets the next product
				store.setNextProduct(new Product(name.toLowerCase(), demandRate, setupCost, unitCost, inventoryCost, sellingPrice));
			}
		} else {
			// full products
			System.out.println("ERROR: You have reached the maximum capacity of three products.");
		}
	}

	/**
	 * This method asks the user for product input and returns the data once validated
	 *
	 * @param string the string for the current input option
	 * @return a double containing the validating user input
	 */
	private double getInput(String string) {
		double input = 0;
		boolean invalid = true;
		// loops until the demand rate is valid
		while (invalid) {
			try {
				System.out.printf("Please enter the %s of the product: ", string);
				input = scanner.nextDouble();
				// checks if the input is negative
				if (input < 0) {
					throw new InputMismatchException();
				}
			} catch (InputMismatchException e) {
				System.out.println("ERROR: The value you entered must be a number and cannot be negative. Please try again.");
				continue;
			}
			invalid = false;
		}
		return input;
	}

	/**
	 * This menu option displays the product data to the user and the replenishment strategy if the parameter is satisfied
	 * @param showReplenishmentStrategy a boolean to check whether the replenishment strategy should be displayed to the user
	 */
	private int menuTwo(boolean showReplenishmentStrategy) {
		int weeks = 0;
		// checks if products are in the system
		if (store.isProductData()) {
			boolean invalid = true;
			// loops until the user enters a valid product
			while (invalid) {
				System.out.println("Please enter the name of the product: ");
				String name = scanner.next();
				// checks if the name is already taken
				if (store.isProductTaken(name)) {
					// displays the replenishment strategy information
					if (showReplenishmentStrategy) {
						// loops until valid
						while (invalid) {
							try {
								System.out.println("Please enter the number of weeks: ");
								weeks = scanner.nextInt();
								// checks if weeks are 0 or less
								if (weeks < 1) {
									throw new InputMismatchException();
								}
								Product product = store.getProduct(name);
								List<Integer> list = store.getReplenishmentStrategy(product, weeks);
								String string = "%10s%20s%12s%12s\n";
								int ordersMade = 0;
								int purchaseAmount = 0;
								int inventoryTotal = 0;
								System.out.printf(string, "Weeks", "Quantity Order", "Demand", "Inventory");
								// loops through the list and outputs each element
								for (int i = 0; i < list.size(); i += 4) {
									// checks if an order has been made within the list
									if (list.get(i + 1) > 0) {
										ordersMade++;
										purchaseAmount += list.get(i + 1);
									}
									inventoryTotal += list.get(i + 3);
									System.out.printf(string, list.get(i), list.get(i + 1), list.get(i + 2), list.get(i + 3));
								}
								double purchaseCost = ordersMade * product.getSetupCost() + purchaseAmount * product.getUnitCost();
								double totalInventoryCost = inventoryTotal * product.getInventoryCost();
								double profit = product.getDemandRate() * weeks * product.getSellingPrice() - (purchaseCost + totalInventoryCost);
								System.out.printf("\nPurchase Cost: $%.2f\nInventory Cost: $%.2f\nTotal Cost: $%.2f\n" +
										"Profit: $%.2f\n\n", purchaseCost, totalInventoryCost,
										purchaseCost + totalInventoryCost, profit);
								invalid = false;
							} catch (InputMismatchException e) {
								System.out.println("ERROR: The week you entered is invalid. Please try again.");
								continue;
							}
						}
					} else {
						// displays the product information
						System.out.println(store.getProduct(name).toString());
						invalid = false;
					}
				} else {
					// not a valid product
					System.out.println("ERROR: The product you entered does not exist. Please try again.");
				}
			}
		} else {
			// no products
			System.out.println("ERROR: There are no products in the system.");
		}
		return weeks;
	}

	/**
	 * This flushes the scanner input and is only used during validation
	 */
	private void flushInput() {
		scanner.nextLine();
	}

}
