package Assignments.src.MatesCoffee.One;

public class Product {

	/**
	 * Member variables
	 */
	private String name;
	private int demandRate;
	private double setupCost;
	private double unitCost;
	private double inventoryCost;
	private double sellingPrice;

	/**
	 * Default constructor
	 */
	public Product() {
		name = "";
		demandRate = 0;
		setupCost = 0;
		unitCost = 0;
		inventoryCost = 0;
		sellingPrice = 0;
	}

	/**
	 * Sets the member variables in the constructor
	 *
	 * @param name the name of the product
	 * @param demandRate the demand rate of the product
	 * @param setupCost the setup cost of the product
	 * @param unitCost the unit cost of the product
	 * @param inventoryCost the totalInventoryCost cost of the product
	 * @param sellingPrice the selling price of the product
	 */
	public Product(String name, int demandRate, double setupCost, double unitCost, double inventoryCost, double sellingPrice) {
		this.name = name;
		this.demandRate = demandRate;
		this.setupCost = setupCost;
		this.unitCost = unitCost;
		this.inventoryCost = inventoryCost;
		this.sellingPrice = sellingPrice;
	}

	/**
	 * A mutator method to set the name of the product
	 *
	 * @param name the name of the product
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * An accessor method to retrieve the name of the product
	 *
	 * @return the name of the product
	 */
	public String getName() {
		return name;
	}

	/**
	 * A mutator method to set the demand rate of the product
	 *
	 * @param demandRate the demand rate of the product
	 */
	public void setDemandRate(int demandRate) {
		this.demandRate = demandRate;
	}

	/**
	 * An accessor method to retrieve the demand rate of the product
	 *
	 * @return the demand rate of the product
	 */
	public int getDemandRate() {
		return demandRate;
	}

	/**
	 * A mutator method to set the setup cost of the product
	 *
	 * @param setupCost the setup cost of the product
	 */
	public void setSetupCost(double setupCost) {
		this.setupCost = setupCost;
	}

	/**
	 * An accessor method to retrieve the setup cost of the product
	 *
	 * @return the setup cost of the product
	 */
	public double getSetupCost() {
		return setupCost;
	}

	/**
	 * A mutator method to set the unit cost of the product
	 *
	 * @param unitCost the unit cost of the product
	 */
	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}

	/**
	 * An accessor method to retrieve the unit cost of the product
	 *
	 * @return the unit cost of the product
	 */
	public double getUnitCost() {
		return unitCost;
	}

	/**
	 * A mutator method to set the totalInventoryCost cost of the product
	 *
	 * @param inventoryCost the totalInventoryCost cost of the product
	 */
	public void setInventoryCost(double inventoryCost) {
		this.inventoryCost = inventoryCost;
	}

	/**
	 * An accessor method to retrieve the totalInventoryCost cost of the product
	 *
	 * @return the totalInventoryCost cost of the product
	 */
	public double getInventoryCost() {
		return inventoryCost;
	}

	/**
	 * A mutator method to set the selling price of the product
	 *
	 * @param sellingPrice the selling price of the product
	 */
	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	/**
	 * An accessor method to retrieve the selling price of the product
	 *
	 * @return the selling price of the product
	 */
	public double getSellingPrice() {
		return sellingPrice;
	}

	/**
	 * An overriden toString method to return the product string
	 *
	 * @return the product string
	 */
	@Override
	public String toString() {
		return String.format("Name: %s\nDemand rate: %s\nSetup cost: %s\nUnit cost: %s\nInventory cost: %s" +
			"\nSelling price: %s\n", name, demandRate, setupCost, unitCost, inventoryCost, sellingPrice);
	}
}