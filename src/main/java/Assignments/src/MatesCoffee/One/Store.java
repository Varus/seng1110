package Assignments.src.MatesCoffee.One;

import java.util.*;

public class Store {

	/**
	 * Member variables
	 */
	private Product product1;
	private Product product2;
	private Product product3;

	/**
	 * Default constructor
	 */
	public Store() {
		product1 = new Product();
		product2 = new Product();
		product3 = new Product();
	}

	/**
	 * Sets the next product
	 *
	 * @param product a product to be added to the store
	 */
	public void setNextProduct(Product product) {
		if (product1.getName().equals("")) {
			// sets the first product
			product1 = product;
		} else if (product2.getName().equals("")) {
			// sets the second product
			product2 = product;
		} else {
			// sets the third product
			product3 = product;
		}
	}

	/**
	 * Sets the existing product with new details
	 *
	 * @param product a product that already exists in the store
	 */
	public void setExistingProduct(Product product) {
		if (product1.getName().equalsIgnoreCase(product.getName())) {
			// sets the first product
			product1 = product;
		}
		if (product2.getName().equalsIgnoreCase(product.getName())) {
			// sets the second product
			product2 = product;
		}
		if (product3.getName().equalsIgnoreCase(product.getName())) {
			// sets the third product
			product3 = product;
		}
	}

	/**
	 * Returns a product that matches the name found in the parameter
	 *
	 * @param name the name of the product
	 * @return the product which matches the name
	 */
	public Product getProduct(String name) {
		if (product1.getName().equalsIgnoreCase(name)) {
			// returns the first product
			return product1;
		}
		if (product2.getName().equalsIgnoreCase(name)) {
			// returns the second product
			return product2;
		}
		if (product3.getName().equalsIgnoreCase(name)) {
			// returns the third product
			return product3;
		}
		return null; // no product associated with the name
	}

	/**
	 * Checks if the product is taken within the store
	 *
	 * @param name the name of the product
	 * @return whether the product name exists in the store
	 */
	public boolean isProductTaken(String name) {
		return product1.getName().equalsIgnoreCase(name) ||
				product2.getName().equalsIgnoreCase(name) ||
				product3.getName().equalsIgnoreCase(name);
	}

	/**
	 * Checks if product data exists in the store
	 *
	 * @return whether or not there is product data
	 */
	public boolean isProductData() {
		return !(product1.getName().equals("") && product2.getName().equals("") && product3.getName().equals(""));
	}

	/**
	 * Determines if the store is full of products
	 *
	 * @return whether or not the store is maxed out of products
	 */
	public boolean isMaxedOut() {
		return !product1.getName().equals("") && !product2.getName().equals("") && !product3.getName().equals("");
	}

	/**
	 * Calculates the optimal order quantity for a product using a product
	 *
	 * @param product the product to calculate the order quantity for
	 * @return the product's order quantity
	 */
	public int getOrderQuantity(Product product) {
		return (int)Math.ceil(Math.sqrt(2 * product.getSetupCost() * product.getDemandRate() / product.getInventoryCost()));
	}

	/**
	 * Calculates the optimal order quantity for a product using the parameters necessary
	 *
	 * @param demandRate the demand rate of a product
	 * @param setupCost the setup cost of a product
	 * @param inventoryCost the inventory cost of a product
	 * @return the optimal order quantity
	 */
	public int getOrderQuantity(double demandRate, double setupCost, double inventoryCost) {
		return (int)Math.ceil(Math.sqrt(2 * setupCost * demandRate / inventoryCost));
	}

	/**
	 * Calculates and returns the replenishment strategy given a product
	 *
	 * @param product the product to calculate the replenishment strategy for
	 * @param weeks the weeks for the replenishment strategy
	 * @return a string containing the replenishment strategy
	 */
	public List<Integer> getReplenishmentStrategy(Product product, int weeks) {
		List<Integer> list = new ArrayList<>();
		int orderQuantity = this.getOrderQuantity(product);
		int finalOrder = (int)Math.ceil((double)product.getDemandRate() * weeks / orderQuantity);
		int ordersMade = 1;
		int inventory = orderQuantity;
		// loops through all the weeks
		for (int i = 1; i <= weeks; i++) {
			// resets the eoq and inventory
			if (inventory - product.getDemandRate() < 0) {
				ordersMade++;
				if (ordersMade == finalOrder) {
					orderQuantity -= (inventory - product.getDemandRate() + orderQuantity) - (weeks - i) * product.getDemandRate();
				}
				else {
					orderQuantity = this.getOrderQuantity(product);
				}
				inventory += orderQuantity - product.getDemandRate();
			} else {
				// reduces the inventory
				inventory -= product.getDemandRate();
			}
			list.addAll(Arrays.asList(i, orderQuantity, product.getDemandRate(), inventory));
			orderQuantity = 0;
		}
		return list;
	}

	/**
	 * This method returns the product with the highest profit for the last known week in the store
	 *
	 * @param weeks the weeks for the replenishment strategy
	 * @return the product with the highest profit
	 */
	public Product getHighestProfit(int weeks) {
		Map<Product, Double> map = new HashMap<>();
		map.put(product1, this.getProfit(product1, weeks, this.getReplenishmentStrategy(product1, weeks)));
		map.put(product2, this.getProfit(product2, weeks, this.getReplenishmentStrategy(product2, weeks)));
		map.put(product3, this.getProfit(product3, weeks, this.getReplenishmentStrategy(product3, weeks)));
		Map.Entry<Product, Double> maxEntry = null;
		for (Map.Entry<Product, Double> entry : map.entrySet()) {
			if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
				maxEntry = entry;
			}
		}
		return maxEntry.getKey();
	}

	/**
	 * Iterates over the list to calculate the profit for the product
	 *
	 * @param product the product to calculate the profit for
	 * @param weeks the weeks being used for the replenishment strategy
	 * @param list the list containing the replenishment strategy
	 * @return the profit for the product given a certain amount of weeks
	 */
	private double getProfit(Product product, int weeks, List<Integer> list) {
		double ordersMade = 0;
		double purchaseAmount = 0;
		double inventoryTotal = 0;
		// loop through the list
		for (int i = 0; i < list.size(); i += 4) {
			// checks if an order has been made within the list
			if (list.get(i + 1) > 0) {
				ordersMade++;
				purchaseAmount += list.get(i + 1);
			}
			inventoryTotal += list.get(i + 3);
		}
		double purchaseCost = ordersMade * product.getSetupCost() + purchaseAmount * product.getUnitCost();
		double totalInventoryCost = inventoryTotal * product.getInventoryCost();
		return product.getDemandRate() * weeks * product.getSellingPrice() - (purchaseCost + totalInventoryCost);
	}

	/**
	 * An overriden toString method to return the store string
	 *
	 * @return the store string
	 */
	@Override
	public String toString() {
		return String.format("Product 1: %s\nProduct 2: %s\n Product 3: %s", product1.toString(),
			product2.toString(), product3.toString());
	}

}
